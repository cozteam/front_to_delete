
import {Token} from "../tokenStorage";

const initialState = {
  report:JSON.parse(localStorage.getItem('reportState'))
}

export function ReportReducers(state = initialState, action) {
  switch (action.type) { 
	case "ADD_REPORT":
		console.log("ADD_REPORT test");
    console.log(action.index);
      localStorage.setItem("reportState", JSON.stringify(action.index));
      return Object.assign({}, state, {
        report:  action.index
      })   
    default:
      	return state
  }
}  