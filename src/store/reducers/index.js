import { combineReducers, applyMiddleware, createStore } from 'redux'
import { UserReducers } from './UserReducers'
import { createBrowserHistory } from 'history'
import { PrintReducers } from './PrintReducers'
import { ReportReducers } from './ReportReducers'
import { connectRouter } from 'connected-react-router'
import createSagaMiddleware from 'redux-saga'
import { routerMiddleware } from 'react-router-redux'

const history = createBrowserHistory()
const sagaMiddleware = createSagaMiddleware()

const middleware = [
  sagaMiddleware,
  routerMiddleware(history)
]

const reducerApp = combineReducers({
  UserReducers,
  PrintReducers,
  ReportReducers,
  router: connectRouter(history),
})

const store = createStore(
  reducerApp,
  applyMiddleware(...middleware)
)



export { store, history }