import { userData } from '../state/UserState'

export function UserReducers(state = userData, action) {
  switch (action.type) { 
	case "UPDATE_USER":
		localStorage.setItem("userData", JSON.stringify(action.index));
	    return Object.assign({}, state, {
	        userData:  action.index
	    })   

    default:
      	return state
  }
}  