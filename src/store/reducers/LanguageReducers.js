
import { initialState } from '../state/Language';

export  function LanguageReducers (state = initialState, action) {
  switch (action.type) {
    case "CHANGE_LANGUAGE":
      return { ...state, language: action.payload.language }
    default :
      return state;
  }
}