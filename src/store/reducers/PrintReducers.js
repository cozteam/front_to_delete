
import { initialState } from '../state/Language';

const printState = {
  print:JSON.parse(localStorage.getItem('printState'))
}

export  function PrintReducers (state = printState, action) {
  switch (action.type) {
    case "ADD_PRINT":
    	localStorage.setItem("printState", JSON.stringify(action.index));
        return { ...state, print: action.index }
    default :
      return state;
  }
}