
export function changeLanguage(langFunction) {
  return dispatch => {
    dispatch({
      type: "CHANGE_LANGUAGE",
      payload: {
        language: langFunction
      }
    });
  }
}