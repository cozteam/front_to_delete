
export function reportActions(index) {
  return { type: "ADD_REPORT", index }
}

export function addTodo(text) {
  return { type: "ADD_TODO", text }
}