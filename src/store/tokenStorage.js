const name = 'userData';

export class Token {
    static getToken() {
        return localStorage.getItem(name);
    }

    static saveToken(token) {
        localStorage.setItem(name, token)
    }

    static destroyToken() {
        window.localStorage.clear()
    }
}
