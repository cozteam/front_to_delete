
import axios from "axios";
import {baseURLTwo} from "../config";
import {userToken} from "./UserToken";

export default axios.create({
   baseURL: `${baseURLTwo}`,
   headers: {
    'Authorization': `Bearer ${userToken}`
   },
   responseType: 'json',
});


