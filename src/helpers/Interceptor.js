
import axios from "axios";
import {baseURL} from "../config";
import {userToken} from "./UserToken";

export default axios.create({
   baseURL: `${baseURL}`,
   headers: {
    'Authorization': `Bearer ${userToken}`
   },
   responseType: 'json',
});

