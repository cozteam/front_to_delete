
import axios from "axios";
import {baseURLFour} from "../config";
import {userToken} from "./UserToken";

export default axios.create({
   baseURL: `${baseURLFour}`,
   headers: {
    'Authorization': `Bearer ${userToken}`
   },
   responseType: 'json',
});


