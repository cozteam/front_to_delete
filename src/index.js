import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux';
import {store, history } from './store/configureStore'
import base from './page/Base';
import report from './page/Report';
import mouseCarts from './page/MouseCarts';
import auth from './page/Auth';
import registration from './page/Registration';
import resetPassword from './page/ResetPassword';
import main from './page/Main';
import print from './page/Print';
import requireAuthentication from './router/Middleware/AuthenticatedComponent'
import ReactDOM from 'react-dom';
import {Helmet} from "react-helmet";


require('./bootstrap');



ReactDOM.render(
  <Provider store={store}>
        <ConnectedRouter history={history}>
          <Helmet title="mats" />

          <Switch>

            <Route  
              exact 
              name='main' 
              path='/' 
              component={main} 
            />

            <Route  
              exact 
              name='auth' 
              path='/auth' 
              component={auth} 
            />

             <Route 
              name='activate' 
              path='/activate/{token}' 
              component={auth} 
            />


            <Route 
              name='registration' 
              path='/registration' 
              component={registration} 
            />

            <Route
              name='resetPassword' 
              path='/reset-password' 
              component={resetPassword} 
            />

            <Route
              name='base' 
              path='/base' 
              component={requireAuthentication(base)} 
            />

            <Route
              name='report' 
              path='/report' 
              component={requireAuthentication(report)} 
            />

            <Route
              name='mouseCarts' 
              path='/mouse-cards' 
              component={requireAuthentication(mouseCarts)} 
            />

            <Route
              name='print' 
              path='/print' 
              component={print} 
            />

            <Route
              name='404' 
              path='*' 
              component={auth} 
            />

          </Switch>
        </ConnectedRouter>
      </Provider>
  , document.getElementById('root'));