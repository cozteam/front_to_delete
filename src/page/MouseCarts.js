import React, { Component } from 'react';
import Header from '../components/Global/Header';
import Spiner from '../components/Global/Spiner';
import API from "../helpers/Interceptor";
import CreateMouse from '../components/Modals/CreateMouse';
import MouseInformation from '../components/Modals/MouseInformation';
import Carts from '../components/MousCarts/Carts';
import {
  BrowserRouter as 
  Router,
  useParams
} from "react-router-dom";




class MouseCarts extends Component {
 
  constructor(props) {
    super(props);
    this.state = {
      activeMouse: [],
      arhiveMouse: [],
      isCreateMouse: '',
      isMouseInformation: '',
      experimentID: '',
      isSpinerActive:true,
    }

    this.selectCarts = this.selectCarts.bind(this);
    this.openMouseInformation = this.openMouseInformation.bind(this);
    this.openCreateMouse = this.openCreateMouse.bind(this);
    this.parsURl = this.parsURl.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }



    componentWillMount() {
      this.parsURl();
    }

    parsURl(){
      let experimentID = this.props.history.location.pathname.replace("/mouse-cards/", '');
      this.setState({
            experimentID:experimentID,
        }, () => {
            this.selectCarts();
        });
    }

    selectCarts(){
      API.get(`/card?experiment_id=${this.state.experimentID}`)
        .then((response) => {
          let activeMouse = [];
          let arhiveMouse = [];
          for (let value of response.data) {
            value.status === "active" ? activeMouse.push(value) : arhiveMouse.push(value);
          }
          this.setState({
            activeMouse:activeMouse,
            arhiveMouse:arhiveMouse,
            isSpinerActive:false
          });
      }); 
    }

    openMouseInformation(){
      this.setState({
        isMouseInformation:true,
      });
    }

    openCreateMouse(){
      this.setState({
        isCreateMouse:true,
      });
    }


    hideModal(){
      this.setState({
        isCreateMouse:false,
        isMouseInformation:false,
      });
    }

 

      render() {
        return (
          <div>
            <CreateMouse
              isActive={this.state.isCreateMouse}
              hideModal={this.hideModal}
              experimentID={this.state.experimentID}
              selectCarts={this.selectCarts}
            />
             <div className="cards">
                <div className="wrapper">
                   <Header/>
                   <section className="cards-wrap cards-active-wrap">
                      <div className="cards-wrap-title">ACTIVE</div>
                      <div className="cards-box-mouse active-box">
                          <Carts 
                            carts={this.state.activeMouse}
                            selectCarts={this.selectCarts}
                          /> 
                          <a 
                            href="#" 
                            id="add-card" 
                            className="card add-card-mouse"
                            onClick={this.openCreateMouse}
                           >
                          <div className="plus-icon"></div>
                        </a>
                      </div>
                   </section>

                   <section className="cards-wrap cards-active-wrap">
                      <div className="cards-wrap-title">ARCHIVE</div>
                      <div className="cards-box active-box">
                          <Carts 
                            carts={this.state.arhiveMouse}
                          /> 
                          <div className="plus-icon"></div>
                      </div>
                   </section>
                    

                </div>
             </div>
            <Spiner
              isSuccess ={this.state.isSpinerActive}
            />
          </div>
      );
    }
}

export default MouseCarts;