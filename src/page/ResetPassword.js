import React, { useState } from 'react';
import Confirm from './../components/Modals/Confirm';
import {baseURLFour} from './../config';
import APIFour from "./../helpers/InterceptorFour";
import {
  BrowserRouter as 
  Router,
  Link,
  useHistory,
} from "react-router-dom";

function ResetPassword(props) {

	const [mail, setMail] = useState("");
	const [errorMail, setErrorMail] = useState(false);
	const [isModalConfirm, setIsModalConfirm] = useState(false);
	const [isSuccessText, setIsSuccessText] = useState("");
	const history = useHistory();

	const validateEmail = (email) => {
		var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    	return re.test(String(email).toLowerCase());
	}

	const passwordReminder = () => {
		(validateEmail(mail)) ? setErrorMail(false) : setErrorMail(true);	

	   	if (validateEmail(mail)) {
	        APIFour.post(`reset_password?email=${mail}`)
	        .then((response) => {
				console.log(response); 
				setIsModalConfirm(true)
				setIsSuccessText(response.data.result);
		        setTimeout(function() {
		           setIsModalConfirm(false);
		        }, 3000);  
		        history.push("/auth")  	     	
	        });
        }
	}
	
	const hideModalConfirm = (email) => {
      	setIsModalConfirm(!isModalConfirm);
    }


    
    return (
    	<div className="main">
    		<div className="main-container">
    			<div className="left-container">
    				<h2 className="container-top-text">Find out</h2>
    				<span className="container-bottom-text">what we can do together</span>
    				<div className="mats-image">
    					<img src="/assets/main/i/MATS.png" alt=""/>
    				</div>
    			</div>
    			<div className="right-container">
					<div className="login">
						<div className="reset-form">
							<div className="login-form-text">Reset password</div>
							<div className="inner-form">
								<div className="login-input">
									<label htmlFor="mail">
									    <i className="fa fa-mail" aria-hidden="true"></i>
									    <input 
										    type="text" 
										    id="mail" 
										    placeholder="E-mail address"
										    value={mail}
										    className={errorMail === 'true' ? 'error-input': ''}
										    onChange={e => setMail(e.target.value)}
									    />
									</label>    
								</div>
								<div className={errorMail == true ? 'error': 'close'}>
			                        Required field
			                    </div> 
			                </div>    
							<div 
								className="main-button"
								onClick={() => passwordReminder()}
							>
								Send Reset
							</div>
							<div className="back-login"><Link to="/registration">Back to Login </Link></div>
						</div>
						<div className="bottom-form-text">Already Have An Account? 
							<Link to="/registration" className="bottom-form-link">Register here</Link>
						</div>
					</div>
				</div>
			</div>
			<Confirm
	            isSuccess={isModalConfirm}
	            hideModal = {hideModalConfirm}
	            isSuccessText={isSuccessText}
	          />
		</div>

    );
    
}






export default (ResetPassword);

