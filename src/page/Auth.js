import React, { useState, useEffect  } from 'react';
import ReactDOM from 'react-dom';
import { useDispatch, useSelector } from 'react-redux'
import {updateUser} from '../store/actions/User'
import { useHistory } from "react-router-dom";
import APIFour from "./../helpers/InterceptorFour";
import {
  BrowserRouter as 
  Router,
  Link,
} from "react-router-dom";

function Auth(props) {

	const [mail, setMail] = useState("");
	const [password, setPassword] = useState("");
	const [errorMail, setErrorMail] = useState(false);
	const [errorPassword, setErrorPassword] = useState(false);
	const dispatch = useDispatch();
	const history = useHistory();

	useEffect(() => {
    	parsURL();
	}, []);
  
    const authUser = () => {
		(password.length) ? setErrorPassword(false) : setErrorPassword(true);	
	   	(validateEmail(mail)) ? setErrorMail(false) : setErrorMail(true);	

	   	if (password.length && validateEmail(mail)) {
	        APIFour.get(`login?email=${mail}&password=${password}`)
	        .then((response) => {
				dispatch(updateUser(response.data)); 
				setTimeout(
					window.location.replace("/base")	
				, 3000); 		
	        });
        }
    }

    const validateEmail = (email) => {
		var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    	return re.test(String(email).toLowerCase());
	}

	const parsURL = () => {
		let url = document.location.href.split('/');
		if(url[3] === "activate"){
			APIFour.get(`/activate/${url[4]}`)
	        .then((response) => {
				console.log(response.data);	
				if (response.data.result === "Success") {
					dispatch(updateUser(response.data)); 
					setTimeout(
						window.location.replace("/base")	
					, 3000); 		
					}
	        });	
		}
	}

        return (
        	<div className="main">
        		<div className="main-container">
        			<div className="left-container">
        				<h2 className="container-top-text">Find out</h2>
        				<span className="container-bottom-text">what we can do together</span>
        				<div className="mats-image">
        					<img src="/assets/main/i/MATS.png" alt=""/>
        				</div>
        			</div>
        			<div className="right-container">
						<div className="login">
							<div className="login-form">
								<div className="login-form-text">login</div>
								<div className="inner-form">
									<div className="login-input">
										<label htmlFor="mail">
										    <i className="fa fa-mail" aria-hidden="true"></i>
										    <input 
											    type="text" 
											    id="mail" 
											    placeholder="E-mail address"
											    value={mail}
											    className={errorMail === 'true' ? 'error-input': ''}
											    onChange={e => setMail(e.target.value)}
										    />
										</label>    
									</div>
									<div className={errorMail == true ? 'error': 'close'}>
				                        Required field
				                    </div> 
									<div className="login-input">
										<label htmlFor="password">
											<i className="fa fa-password" aria-hidden="true"></i>
											<input 
												type="text" 
												id="password" 
												placeholder="Password"
												value={password}
												className={errorPassword === 'true' ? 'error-input': ''}
												onChange={e => setPassword(e.target.value)}
											/>
									    </label>
									</div>
									<div className={errorPassword == true ? 'error': 'close'}>
				                        Required field
				                    </div>
			                    </div>
								<div 
									className="main-button"
									onClick={() => authUser()}
								>
									Login
								</div>
								<div className="forgot-password">
									<Link to='reset-password'>
										Forgot Password?
									</Link>	
								</div>
							</div>

							<div className="bottom-form-text">
								Don’t Have An Account? 
								<Link to='registration' className="bottom-form-link" >Register here</Link>
							</div>
						</div>
					</div>
        		</div>
        	</div>			
        );
  
}






export default(Auth);




