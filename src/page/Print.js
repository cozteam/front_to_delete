import React  from 'react';
import Header from '../components/Global/Header';
import { useSelector } from 'react-redux';



 function Print(props) {
    const print = useSelector(state => state.PrintReducers.print);
    
    return (
        <div className="cards">
          <div className="wrapper">
            <Header/>
             <section className="cards-wrap cards-active-wrap">
                <div className="cards-wrap-title">Report</div>
                <div className="cards-box active-box">
                    {print}      
                  <div className="plus-icon"></div>
                </div>
             </section >

          </div>
         </div>
    );
}

export default (Print);