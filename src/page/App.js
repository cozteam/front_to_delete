import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div>

    <div class="loader-overlay">
        <div class="popup loader lds-dual-ring"></div>
    </div>
    <div id="overlay" class="overlay">
        <p id="random_nums"></p>


        <div class="popup yes-no-popup archive-submit-popup">
            <p>Are you sure?</p>
            <div class="yes-no">
                <div class="submit-btn yes-btn">Yes</div>
                <div class="submit-btn no-btn">No</div>
            </div>

        </div>

        <div class="popup yes-no-popup delete-submit-popup">
            <p>Are you sure?</p>
            <div class="yes-no">
                <div class="submit-btn yes-btn">Yes</div>
                <div class="submit-btn no-btn">No</div>
            </div>

        </div>

        <div class="popup experiment-download-select-popup">
            <p>Please select experiment</p>
            <select>
            
            </select>
            <div class="yes-no">
                <div class="submit-btn yes-btn">Ok</div>
                <div class="submit-btn no-btn">Cancel</div>
            </div>
        </div>

        <div class="popup experiment-report-select-popup">
            <p>Please select experiment</p>
            <select>
            
            </select>
            <div class="yes-no">
                <div class="submit-btn yes-btn">Ok</div>
                <div class="submit-btn no-btn">Cancel</div>
            </div>
        </div>


<!-- block for add card start -->
    <div class="add">
  <div id="close-add-btn" class="close-btn popup__close-btn"></div>
  <form class="form-add">
    <div class="top-form">
      <div class="form-top-box">
        <div class="form-top-title">Sensor ID</div>
        <div class="form-el">
          <input onkeypress="return this.value.length < 10;" oninput="if(this.value.length>=5) { this.value = this.value.slice(0,10); }" required type="number" class="sensor_id">
        </div>
      </div>
      <div class="form-top-box">
        <div class="form-top-title">Mouse ID</div>
        <div class="form-el">
          <input onkeypress="return this.value.length < 10;" oninput="if(this.value.length>=5) { this.value = this.value.slice(0,10); }" required type="number" class="mouse_id">
        </div>
      </div>
    </div>
    <div class="form-gender">
      <div class="form-gender-item">
        <input id="form_male" type="radio" name="sex" class="sex-input" checked>
        <label for="form_male">
          <span class="radio"></span>
          <span class="label">Male</span>
        </label>
      </div>
      <div class="form-gender-item">
        <input id="form_female" type="radio" name="sex" class="sex-input">
        <label for="form_female">
          <span class="radio"></span>
          <span class="label">Female</span>
        </label>
      </div>
    </div>
    <div class="form-el-wrap">
      <label for="form_date-birth">Date of Birth</label>
      <input required type="text" class="dob-input date-picker">
    </div>
    <div class="form-el-wrap">
      <label for="form_date-start-analysis">Start Analysis</label>
      <input required type="text" class="dos-input date-picker">
    </div>
    <div class="form-el-wrap">
      <label for="form_date-stop-analysis">Stop Analysis</label>
      <input required type="text" class="doe-input date-picker">
    </div>
    <div class="form-el-wrap">
      <label for="form_treatment">Treatment</label>
      <input onkeypress="return this.value.length < 19;" oninput="if(this.value.length>=5) { this.value = this.value.slice(0,19); }" required type="text" class="treatment-input">
    </div>
    <div class="form-el-wrap">
      <label for="form_genotype">Genotype</label>
      <input onkeypress="return this.value.length < 19;" oninput="if(this.value.length>=5) { this.value = this.value.slice(0,19); }" required type="text" class="genotype-input">
    </div>
  <div class="form-el-wrap">
      <label for="form_experiment_namet">Experiment name</label>
      <input onkeypress="return this.value.length < 19;" oninput="if(this.value.length>=5) { this.value = this.value.slice(0,19); }" required type="text" class="experiment_name-input">
    </div>
    <div class="form-el-wrap">
      <label for="form_cageId">Cage ID</label>
      <input onkeypress="return this.value.length < 10;" oninput="if(this.value.length>=5) { this.value = this.value.slice(0,10); }" required type="number" class="cage_id-input">
    </div>
      <div class="form-el-wrap">
      <label>Computer</label>
           <select required name="computer" class="computer-input">
               
</select>
    </div>

    <div class="form-textarea-wrap">
      <textarea onkeypress="return this.value.length < 399;" oninput="if(this.value.length>=5) { this.value = this.value.slice(0,399); }" placeholder="Comments" class="comments-input"></textarea>
    </div>
    <div class="form-btn-wrap">
      <div class="submit-btn add-card-btn">CREATE</div>
        <button type="submit" style="display:none;"></button>
    </div>
  </form>
</div>

<!-- block for add card end -->
<!-- block for archive card start -->
    
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<div data-key="" class="card-details">

  <div id="close-archive-btn" class="close-btn popup__close-btn"></div>
  <form action="#" class="form-archive">
    <div class="archive-header">
      <div class="arc-head-time-wrap">

        <p class="arc-head-time-btn active" data-time="days">Days</p>
        <p class="arc-head-time-btn" data-time="hours">Hours</p>
      </div>
      <div class="arc-head-desc-wrap">
        <div class="arc-head-desc-item">
          <div class="arc-head-desc-title">Sensor ID</div>
          <div class="arc-head-desc-val sens-id"></div>
        </div>
        <div class="arc-head-desc-item">
          <div class="arc-head-desc-title">Mouse ID</div>
          <div class="arc-head-desc-val mouse-id"></div>
        </div>
        <div class="arc-head-desc-item">
          <div class="arc-head-desc-title">Cage ID</div>
          <div class="arc-head-desc-val cage-id"></div>
        </div>
      </div>
      <div class="arc-head-type-wrap">
        <a href="#" class="arc-head-type-btn active" data-type="bar">Bar</a>
        <a href="#" class="arc-head-type-btn" data-type="circadian" data-toggle="no">Actogram</a>
        <a href="#" class="arc-head-type-btn" data-type="periodogram" data-toggle="no">FFT</a>
      </div>
    </div>
    <div class="archive-img-wrap">
      
<div id="plotly_dash" class="archive-img archive-img-bar active"><div style="
    position: relative;
    padding-bottom: 10.0%;
    height: 0;
    overflow:hidden;
    ">
  <iframe src="/django_plotly_dash/app/SimpleExample/initial/dpd-initial-args-bfa0c04ded9e45e7b52c1e995a6597c7/" style="
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    "frameborder="0"></iframe>
</div>
</div>
      
    </div>
    <div class="archive-edit">
      <div class="archive-edit-block">
        <div class="archive-edit-row">
          <span class="archive-edit-title">Total distance</span>
          <span id="total-distance" class="archive-edit-value distance-val"></span>
        </div>
        <div class="archive-edit-row">
          <span class="archive-edit-title">Average speed</span>
          <span id="avg-speed" class="archive-edit-value distance-val"></span>
        </div>
      </div>
      <div class="archive-edit-block">
        <div class="archive-edit-toprow">
          <span class="archive-edit-top-title sex-val"></span>
          <span class="archive-edit-top-title treatment-val"></span>
          <span class="archive-edit-top-title genotype-val"></span>
        </div>
        <div class="archive-edit-row">
          <span class="archive-edit-title">Age</span>
          <span class="archive-edit-value"></span>
        </div>
        <div class="archive-edit-row">
          <span class="archive-edit-title">Days recards</span>
          <span class="archive-edit-value"></span>
        </div>
        <div class="archive-edit-row">
          <span class="archive-edit-title">Date of start</span>
          <span class="archive-edit-input"></span>
        </div>
        <div class="archive-edit-row">
          <span class="archive-edit-title">Date of end</span>
          <input value="" 
            class="archive-edit-input doe-edit">
        </div>
        <textarea class="archive-edit-textarea comments-val" 
          placeholder="Comments"></textarea>
      </div>
    </div>
    <div class="archive-btn-wrap">
      
      <div class="submit-btn submit-delete-btn">Delete</div>

      <button type="submit" style="display:none;"></button>
    </div>
    <div style="display: none" class="card_info">
      <p class="card-details__card-id"></p>
    </div>
  </form>
  <div class="portrait-warning">Please rotate your device to landscape orientation</div>
</div>

<!-- block for archive card end -->
    </div>
    <div class="wrapper">



        <header class="cards-header">
    <div id="js-particle-field" class="particle-field"></div>
    <div id="js-icosa-field" class="icosa-field"></div>
    <div class="cards-header-content">
        <a href="/" class="cards-main-logo"></a>
        <nav class="cards-navigation">
            <script>
            </script>
<!--            <div class="cards-nav-item connections-count">Connections count: <span>0</span></div>-->
            <div style="cursor: pointer" class="cards-nav-item visual-link cards-nav-print">PRINT</div>
            <div style="cursor: pointer" class="cards-nav-item visual-link cards-nav-report">REPORT</div>
            <div style="cursor: pointer" class="cards-nav-item visual-link cards-nav-download">DOWNLOAD</div>
            
            <p class="cards-nav-item visual-link cards-nav-person" style="text-transform: uppercase">Victor
            </p>
            
            <a href="/logout/" class="cards-nav-item visual-link cards-nav-logout"
                style="text-transform: uppercase">LOGOUT</a>
        </nav>
    </div>
</header>
        <section class="cards-wrap cards-active-wrap">
            <div class="cards-wrap-title">ACTIVE</div>
            <div class="cards-box active-box">
                
                <a href="#" id="add-card" class="card add-card">
                    <div class="plus-icon"></div>
                </a>
            </div>
        </section>
        <section class="cards-wrap cards-archive-wrap">
            <div class="cards-wrap-title">ARCHIVE</div>
            <div class="cards-box archive-box">
                
            </div>
        </section>
    </div>
</body>






      </div>
    );
  }
}

export default App;