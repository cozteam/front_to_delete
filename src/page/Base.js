import React, { PureComponent } from 'react';
import Header from '../components/Global/Header';
import Spiner from '../components/Global/Spiner';
import FileBlue from '../components/Base/FileBlue';
import ExperimentCreation from '../components/Modals/ExperimentCreation';
import API from "../helpers/Interceptor";
import {reportActions} from '../store/actions/ReportActions'
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

class Base extends PureComponent {

 
   constructor(props) {
    super(props);
    this.state = {
      activeCarts: [],
      archiveCarts: [],
      isActive:'',
      computers: [],
      isSpinerActive:true,
    }

    this.selectCarts = this.selectCarts.bind(this);
    this.openModal = this.openModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }

    componentDidMount() {

      this.selectCarts();
    }

    selectCarts(){
        API.get(`/experiment?user_id=${this.props.userID}`)
          .then((response) => {
            let activeCarts = [];
            let archiveCarts = [];  
            for (let value of response.data) {
              value.status === "active" ? activeCarts.push(value) : archiveCarts.push(value);
            }
            this.setState({ 
              activeCarts: activeCarts,
              archiveCarts: archiveCarts,
              isSpinerActive:false
            })
        });
    }


    openModal(){
      this.setState({
        isActive:true,
      });
    }
    hideModal(){
      this.setState({
        isActive:false,
      });
    }


      render() {
         return (
          <React.Fragment>

            <ExperimentCreation 
              isActive={this.state.isActive}
              hideModal={this.hideModal}
              selectAllCarts={this.selectCarts}
            />            

            <div className="cards">

              <div id="overlay" className="overlay">
                 <p id="random_nums"></p>
                 <div className="popup yes-no-popup archive-submit-popup">
                    <p>Are you sure?</p>
                    <div className="yes-no">
                       <div className="submit-btn yes-btn">Yes</div>
                       <div className="submit-btn no-btn">No</div>
                    </div>
                 </div>
                 <div className="popup yes-no-popup delete-submit-popup">
                    <p>Are you sure?</p>
                    <div className="yes-no">
                       <div className="submit-btn yes-btn">Yes</div>
                       <div className="submit-btn no-btn">No</div>
                    </div>
                 </div>
                 <div className="popup experiment-download-select-popup">
                    <p>Please select experiment</p>
                    <select>
                    </select>
                    <div className="yes-no">
                       <div className="submit-btn yes-btn">Ok</div>
                       <div className="submit-btn no-btn">Cancel</div>
                    </div>
                 </div>
                 <div className="popup experiment-report-select-popup">
                    <p>Please select experiment</p>
                    <select>
                    </select>
                    <div className="yes-no">
                       <div className="submit-btn yes-btn">Ok</div>
                       <div className="submit-btn no-btn">Cancel</div>
                    </div>
                 </div>

                 <div data-key="" className="card-details">
                    <div id="close-archive-btn" className="close-btn popup__close-btn"></div>
                    <form action="#" className="form-archive">
                       <div className="archive-header">
                          <div className="arc-head-time-wrap">
                             <p className="arc-head-time-btn active" data-time="days">Days</p>
                             <p className="arc-head-time-btn" data-time="hours">Hours</p>
                          </div>
                          <div className="arc-head-desc-wrap">
                             <div className="arc-head-desc-item">
                                <div className="arc-head-desc-title">Sensor ID</div>
                                <div className="arc-head-desc-val sens-id"></div>
                             </div>
                             <div className="arc-head-desc-item">
                                <div className="arc-head-desc-title">Mouse ID</div>
                                <div className="arc-head-desc-val mouse-id"></div>
                             </div>
                             <div className="arc-head-desc-item">
                                <div className="arc-head-desc-title">Cage ID</div>
                                <div className="arc-head-desc-val cage-id"></div>
                             </div>
                          </div>
                          <div className="arc-head-type-wrap">
                             <a href="#" className="arc-head-type-btn active" data-type="bar">Bar</a>
                             <a href="#" className="arc-head-type-btn" data-type="circadian" data-toggle="no">Actogram</a>
                             <a href="#" className="arc-head-type-btn" data-type="periodogram" data-toggle="no">FFT</a>
                          </div>
                       </div>
                       <div className="archive-img-wrap">
                       </div>
                       <div className="archive-edit">
                          <div className="archive-edit-block">
                             <div className="archive-edit-row">
                                <span className="archive-edit-title">Total distance</span>
                                <span id="total-distance" className="archive-edit-value distance-val"></span>
                             </div>
                             <div className="archive-edit-row">
                                <span className="archive-edit-title">Average speed</span>
                                <span id="avg-speed" className="archive-edit-value distance-val"></span>
                             </div>
                          </div>
                          <div className="archive-edit-block">
                             <div className="archive-edit-toprow">
                                <span className="archive-edit-top-title sex-val"></span>
                                <span className="archive-edit-top-title treatment-val"></span>
                                <span className="archive-edit-top-title genotype-val"></span>
                             </div>
                             <div className="archive-edit-row">
                                <span className="archive-edit-title">Age</span>
                                <span className="archive-edit-value"></span>
                             </div>
                             <div className="archive-edit-row">
                                <span className="archive-edit-title">Days recards</span>
                                <span className="archive-edit-value"></span>
                             </div>
                             <div className="archive-edit-row">
                                <span className="archive-edit-title">Date of start</span>
                                <span className="archive-edit-input"></span>
                             </div>
                             <div className="archive-edit-row">
                                <span className="archive-edit-title">Date of end</span>
                                <input  className="archive-edit-input doe-edit" />
                             </div>
                             <textarea className="archive-edit-textarea comments-val" 
                                placeholder="Comments"></textarea>
                          </div>
                       </div>
                       <div className="archive-btn-wrap">
                          <div className="submit-btn submit-delete-btn">Delete</div>
                          <button type="submit" ></button>
                       </div>
                       <div  className="card_info">
                          <p className="card-details__card-id"></p>
                       </div>
                    </form>
                    <div className="portrait-warning">Please rotate your device to landscape orientation</div>
                 </div>
              </div>
              <div className="wrapper">
                <Header/>
                 <section className="cards-wrap cards-active-wrap">
                    <div className="cards-wrap-title">ACTIVE</div>
                    <div className="cards-box active-box">
                      <FileBlue 
                        carts={this.state.activeCarts}
                        selectCarts={this.selectCarts}
                      />  
                      <a 
                        href="#" 
                        id="add-card" 
                        className="card add-card"
                        onClick={this.openModal}
                      >
                          <div className="plus-icon"></div>
                      </a>
                    </div>
                 </section >
                 <section className="cards-wrap cards-archive-wrap">
                    <div className="cards-wrap-title">ARCHIVE</div>
                    <div className="cards-box archive-box">
                      <FileBlue
                        carts={this.state.archiveCarts} 
                        selectCarts={this.selectCarts}
                      />
                    </div>
                 </section>
              </div>
            </div>
            <Spiner
             isSuccess ={this.state.isSpinerActive}
            />
          </React.Fragment>
        );
  }
}

const mapStateToProps = (state, props) => {
   return {
      userID: state.UserReducers.user_id,
   }
 }
 
 const mapDispatchToProps = (dispatch) => {
   return {
   }
 }

export default withRouter(connect(mapStateToProps, null)(Base));