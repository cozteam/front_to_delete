import React, { useState } from 'react';
import Slider from '../components/Main/Slider';
import MakeOrder from '../components/Modals/MakeOrder';
import Particles from 'react-particles-js';

import {
  BrowserRouter as Router, 
  Link,
} from "react-router-dom";
 

function Main(props) {
	const [priceWheel, setPriceWheel] = useState('15');
	const [countWheel, setCountWheel] = useState(1);
	const [systemName, setSystemName] = useState("monthlyPaid");
	const [isModalOrder, setIsModalOrder] = useState(false);
	const [isActiveModalOrder, setIsActiveModalOrder] = useState("monthlyPaid");



	const hideModal = () => {
      setIsModalOrder(false);
    }

	
	const quantityManagement = (data) => {
		if (data === "down" && countWheel != 1) {
			 setCountWheel(countWheel - 1)
		}
		if (data === "add") {
			setCountWheel(countWheel + 1)
		}
	}
    
        return (
        	<React.Fragment>
				<div className="wrapper">
				    <header className="header">
				       
				        <div className="header-content">
				            <a href="/" className="main-logo"></a>
				            <Link to="/auth" className="navigation">
				                <p className="nav-item visual-link nav-person open-popup">PERSONAL ACCOUNT</p>
				            </Link>

				        </div>
				         <Particles />
				    </header>
				    <section className="main-text-wrap">
				        <div className="main-text-container container">
				            <div className="main-text-block main-text-autoprotocol">
				                <div className="main-text-title">What is MATS?</div>
				                <p>Our solution is the most cost-effective way of locomotor behavior monitoring. We can benefit researchers working in circadian biology, neurodegeneration, metabolism etc. The system is robust and  easy to use. Animals are exposed to enriched environment which increases their health status as well as social and cognitive functions. At the same time the measurements of activity during the day provide the phenotypic monitoring of the mice. 
				                </p>
				                <p>Analysis of voluntary activity provides a more natural way of locomotor phenotype assessment.</p>
				            </div>
				            <div className="main-text-block main-text-started">
				                <div className="main-text-title">Get Started!</div>
				                <p>The fastest way to get started is go through the MATS web-interface and check all functions. </p>
				                <p>Choose quantity of wheels and make an order, we will send you disassemble wheels and you have to assemble it using only screw, no gluing.</p>
				                <p>There' s a support to talk to if you're looking for ideas or run into trouble getting up andrunning.</p>
				                <p>Powered by : <a class="next-site" href='http://vinnikov.science'>vinnikov.science</a></p>
				            </div>
				        </div>
				    </section>

				    <section className="main-video-wrap">
				        <div className="main-video-carousel">
				            <Slider/>
				        </div>
				    </section>

				    <section className="main-cards-wrap container">
				        <div className="card-wrap card_wheel">
				            <div className="card-title">WHEEL</div>
				            <div className="card-box">
				                <div className="card-box-wrap">
				                    <div className="card-img">
				                        <img src="/assets/main/i/wheel.jpg"/>
				                    </div>
				                    <div className="card-text-wrap">
				                        <div className="card-text-title">We provide:</div>
				                        <div className="card-text-list">
				                            <div className="card-text-item">Component for assembling</div>
				                            <div className="card-text-item">Instruction</div>
				                            <div className="card-text-item">No gluing</div>
				                        </div>
				                        <div className="card-text-price-wrap">
				                            <div className="card-price">{priceWheel * countWheel}<span>$</span></div>
				                            <div className="card-counter">
				                                <span 
				                                	className="counter-decrease visual-link"
				                                	onClick={() => quantityManagement("down")}
				                                >
				                                </span>
				                                <span id="wheel-counter" className="counter-number">{countWheel}</span>
				                                <span 
				                               		className="counter-increase visual-link"
				                               		onClick={() => quantityManagement("add")}
				                                >
				                                </span>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				        <div className="card-wrap card_system">
				            <div className="card-title">SYSTEM</div>
				            <div className="card-box">
				                <div className="card-box-wrap">
				                    <div className="card-img">
				                        <img src="/assets/main/i/notebook.png"/>
				                    </div>
				                    <div className="card-text-wrap">
				                        <div className="card-text-title">Web interface for analysis:</div>
				                        <div className="card-text-list">
				                            <div className="card-text-item">Distance</div>
				                            <div className="card-text-item">Speed</div>
				                            <div className="card-text-item">Circadian rhythms</div>
				                            <div className="card-text-item">Statistical analysis</div>
				                        </div>
				                    </div>
				                </div>
				                <div className="card-btns">
				                    <div 
				                   	 	class={systemName == "monthlyPaid" ? 'card-button active-button': 'card-button'}
										onClick={e => setSystemName("monthlyPaid")}
				                    >
				                        <div className="card-button-title">Monthly Paid</div>
				                        <div className="card-button-price">5<span>$ / wheel</span></div>
				                        <div className="card-button-desc">60<span>$ / wheel per year</span></div>
				                    </div>
				                    <div 
				                   		class={systemName == "annuallyPaid" ? 'card-button active-button': 'card-button'}
										 onClick={e => setSystemName("annuallyPaid")}
				                    >
				                        <div className="card-button-title">Annually Paid</div>
				                        <div className="card-button-price">3.5<span>$ / wheel</span></div>
				                        <div className="card-button-desc">42<span>$ / wheel per year</span></div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </section>
				    <section 
					    className="main-order-wrap"
					    onClick={e => setIsModalOrder(true)}
				    >
				        <a className="order-btn">MAKE ORDER</a>
				    </section>

				</div>
				<footer className="footer">
				    <div className="footer-contact">
				        <a className="contact-link visual-link" href="mailto:info@smart-mats.com">info@smart-mats.com</a>
				    </div>
				</footer>
				<MakeOrder
					isActive={isModalOrder}
              		hideModal={hideModal}
				/>
        	</React.Fragment>
        );
    
}






export default (Main);

