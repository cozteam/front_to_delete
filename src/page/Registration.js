import React, { useState, useEffect } from 'react';
import Confirm from './../components/Modals/Confirm';
import APIFour from "./../helpers/InterceptorFour";
import {
  BrowserRouter as 
  Router,
  Link,
  useHistory,  
} from "react-router-dom";


function Registration(props) {

	const [mail, setMail] = useState("");
	const [password, setPassword] = useState("");
	const [userName, setUserName] = useState("");
	const [errorMail, setErrorMail] = useState(false);
	const [errorPassword, setErrorPassword] = useState(false);
	const [errorUserName, setErrorUserName] = useState(false);
	const [isModalConfirm, setIsModalConfirm] = useState(false);
	const [isSuccessText, setIsSuccessText] = useState("");
	const history = useHistory();




	const registrationUser = async () => {
		await(userName.length) ? setErrorUserName(false) : setErrorUserName(true);
		await(password.length) ? setErrorPassword(false) : setErrorPassword(true);	
	   	await(validateEmail(mail)) ? setErrorMail(false) : setErrorMail(true);	
	
	   	if (userName.length &&password.length && validateEmail(mail)) {
	        APIFour.post(`registration?email=${mail}&username=${userName}&password=${password}`)
	        .then((response) => {
        		setIsModalConfirm(true)
				response.data.result === "Error" ? setIsSuccessText(response.data.msg): setIsSuccessText(response.data.result);
		        setTimeout(function() {
		         	setIsModalConfirm(false);
				    response.data.result === "Error" ? setIsModalConfirm(false) : history.push("/auth") ;
		        }, 3000);	
	        });
        }
    }


	const hideModalConfirm = (email) => {
      setIsModalConfirm(!isModalConfirm);
    }


    const validateEmail = (email) => {
		var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    	return re.test(String(email).toLowerCase());
	}


    return (
       <div className="main">
    		<div className="main-container">
    			<div className="left-container">
    				<h2 className="container-top-text">Find out</h2>
    				<span className="container-bottom-text">what we can do together</span>
    				<div className="mats-image">
    					<img src="/assets/main/i/MATS.png" alt=""/>
    				</div>
    			</div>
    			<div className="right-container">
					<div className="login">
						<div className="registration-form">
							<div className="login-form-text">Register</div>

							<div className="inner-form">	
								<div className="login-input">
									<label htmlFor="userName">
									    <i className="fa fa-username" aria-hidden="true"></i>
									    <input 
										    type="text" 
										    id="userName" 
										    placeholder="Username"
										    value={userName}
										    className={errorUserName == true ? 'error-input': ''}
										    onChange={e => setUserName(e.target.value)}
									    />
									</label>    
								</div>
								<div className={errorUserName == true ? 'error': 'close'}>
			                        Required field
			                    </div>

								<div className="login-input">
									<label htmlFor="mail">
									    <i className="fa fa-mail" aria-hidden="true"></i>
									    <input 
										    type="text" 
										    id="mail" 
										    placeholder="E-mail address"
										    value={mail}
										    className={errorMail == true ? 'error-input': ''}
										    onChange={e => setMail(e.target.value)}
									    />
									</label> 
								</div>
								<div className={errorMail == true ? 'error': 'close'}>
			                        Required field
			                    </div> 


								<div className="login-input">
									<label htmlFor="password">
										<i className="fa fa-password" aria-hidden="true"></i>
										<input 
											type="password" 
											id="password" 
											placeholder="Password"
											value={password}
											className={errorPassword == true ? 'error-input': ''}
											onChange={e => setPassword(e.target.value)}
										/>
								    </label>
								</div>
								<div className={errorPassword == true ? 'error': 'close'}>
			                        Required field
			                    </div>
							</div>


							<div 
								className="main-button"
								onClick={() => registrationUser()}
							>
								Register
							</div>

						</div>

						<div className="bottom-form-text">Already Have An Account? 
							<Link to="/auth" className="bottom-form-link">Login</Link>
						</div>
					</div>
				</div>
    		</div>
    		<Confirm
	            isSuccess={isModalConfirm}
	            hideModal = {hideModalConfirm}
	            isSuccessText={isSuccessText}
	          />
    	</div>	
    );
  
}





export default (Registration);

