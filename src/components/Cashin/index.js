import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Header from '../../components/global/Header'
import Menu from '../../components/global/Menu'
import Footer from '../../components/global/Footer'


class Cashin extends Component {
    render() {
        return (
			<div class="container-fluid">
			   <div class="row">
			      <Header/>
			      <div class="right-column">
			         <Menu/>
			         <main class="main-content p-4" role="main">
			            <div class="row">
			               <div class="col-md-12">
			                  <h3>Пополнение кошелька</h3>
			               </div>
			               <div class="col-md-12">
			                  <div class="row">
			                     <div class="col-md-6 col-lg-6 col-xl-3 mb-3">
			                        <div class="card card-tile card-xs bg-danger bg-gradient text-center">
			                           <div class="card-body p-4">
			                              <div class="tile-left">
			                                 <i class="batch-icon batch-icon-wallet batch-icon-xxl"></i>
			                              </div>
			                              <div class="tile-right">
			                                 <div class="tile-number">&#8376;1500</div>
			                                 <div class="tile-description">Текуший Баланс</div>
			                              </div>
			                           </div>
			                        </div>
			                     </div>
			                     <div class="col-md-6 col-lg-6 col-xl-3 mb-3">
			                        <div class="card card-tile card-xs bg-secondary bg-gradient text-center">
			                           <div class="card-body p-4">
			                              <div class="tile-left">
			                                 <i class="batch-icon batch-icon-in batch-icon-xxl"></i>
			                              </div>
			                              <div class="tile-right">
			                                 <div class="tile-number">&#8376;500</div>
			                                 <div class="tile-description">На подтверждении</div>
			                              </div>
			                           </div>
			                        </div>
			                     </div>
			                  </div>
			                  <div id="accordion" role="tablist" aria-multiselectable="true">
			                     <div class="card">
			                        <div class="card-header" role="tab" id="headingTwo">
			                           <h5 class="mb-0">
			                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			                                 <center>Правила пополнения баланса кошелька</center>
			                              </a>
			                           </h5>
			                        </div>
			                        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
			                           <div class="card-body">
			                              <h5>KASPI GOLD</h5>
			                              Минимальная сумма пополнения равняется стоимости одного билета Telebingo - 300 тенге.<br></br>
			                              Переводы между клиентами KASPI - без комиссии. Обязательно при переводе в сообщении получателю необходимо указать ваш ID  на сайте bingos.kz. <br></br>
			                              При переводе на карту KASPI GOLD с карт других банков может удерживаться комиссия. Необходимо это учесть, чтобы сумма поступления была не менее 300 тенге.<br></br>
			                              Деньги в KZT на Ваш баланс bingos.kz поступят после ручного подтверждения оператором. Время работы оператора с 10:00 до 22:00. <br></br>Также, Вы можете автоматизировать взнос в банк синдиката, отметив соответствующую галочку в окне при пополнении. При этом, после подтверждения о поступлении денег, автоматом, с Вашего баланса будет произведен взнос в синдикат ближайшей игры. <br></br>		
			                              <h5>Яндекс.Деньги</h5>
			                              Деньги в KZT на Ваш баланс bingos.kz поступят в режиме онлайн за минусом следующих комиссий и по курсу конвертаций валют:<br></br>
			                              1. При пополнении с вашего Яндекс кошелка по тарифу Яндекса - 0,5% или 2% при оплате банковской картой.<br></br>
			                              2. *Комиссия за перевод с моего Яндекса счета на карту банка РК в рублях - 3% + 45 рублей (тариф Яндекса).<br></br>
			                              3. .
			                              <br></br>
			                              *Примечание: Пополнение и вывод осуществляется через банк в РК.
			                           </div>
			                        </div>
			                     </div>
			                  </div>
			                  <br></br>
			                  <br></br>
			                  <button type="button" class="btn btn-orange btn-lg btn-block" data-toggle="modal" data-target="#exampleModalCenter1">KASPI GOLD
			                  </button>
			                  <button type="button" class="btn btn-danger btn-lg btn-block" data-toggle="modal" data-target="#exampleModalCenter">Яндекс.Деньги
			                  </button>							
			                  <br></br>
			                  <h7>
			                     <center>При пополнении счета сайт bingos.kz не взимает комиссию.</center>
			                  </h7>
			                  <h7>
			                     <center>Отправляя деньги на Ваш баланс Вы соглашаетесь с выше перечисленными расходами на перевод и конвертацию валют.</center>
			                  </h7>
			                  <br></br>
			               </div>
			            </div>
			            // Modal Денежный взнос 
			            <div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			               <div class="modal-dialog modal-dialog-centered" role="document">
			                  <div class="modal-content">
			                     <div class="modal-header">
			                        <h5 class="modal-title" id="exampleModalLongTitle">Банковский перевод на KASPI GOLD</h5>
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                        <span aria-hidden="true">&times;</span>
			                        </button>
			                     </div>
			                     <div class="modal-body">
			                        <h5>Номер телефона: 7(708)709-XXXX.</h5>
			                        <p>После перевода денег в приложении Kaspi укажите данную сумму в нижнем поле и нажмите кнопку"Сохранить".</p>
			                        <div class="input-group">
			                           <div class="input-group-prepend">
			                              <span class="input-group-text">KZT</span>
			                           </div>
			                           <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" />
			                           <div class="input-group-append">
			                              <span class="input-group-text">.00</span>
			                           </div>
			                        </div>
			                        <br></br>
			                        <div class="custom-control custom-checkbox mb-3">
			                           <input type="checkbox" class="custom-control-input" id="customControlValidation2" />
			                           <label class="custom-control-label" for="customControlValidation2"><strong>Автовзнос в синдикат после подтверждения</strong></label>
			                        </div>
			                     </div>
			                     <div class="modal-footer">
			                        <button type="button" class="btn btn-primary">Сохранить</button>
			                     </div>
			                  </div>
			               </div>
			            </div>
			            //Modal Денежный взнос
			            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			               <div class="modal-dialog modal-dialog-centered" role="document">
			                  <div class="modal-content">
			                     <div class="modal-header">
			                        <h5 class="modal-title" id="exampleModalLongTitle">Яндекс.Деньги или Банковские карты</h5>
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                        <span aria-hidden="true">&times;</span>
			                        </button>
			                     </div>
			                     <div class="modal-body">
			                        <iframe src="https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=%D0%9F%D0%BE%D0%BF%D0%BE%D0%BB%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%B1%D0%B0%D0%BB%D0%B0%D0%BD%D1%81%D0%B0%20%20ID%3A%200001&targets-hint=&default-sum=&button-text=11&payment-type-choice=on&mail=on&hint=&successURL=http%3A%2F%2Fozlotto.online%2Findex.html&quickpay=shop&account=410011266390797" width="100%" height="222" frameborder="0" allowtransparency="true" scrolling="no"></iframe>
			                     </div>
			                  </div>
			               </div>
			            </div>
			         </main>
			         <Footer/>
			      </div>
			   </div>
			</div>
        );
    }
}

export default Cashin;


if (document.getElementById('Cashin')) {
    ReactDOM.render(<Cashin />, document.getElementById('Cashin'));
}