import React from 'react';


function Spiner(props) {
   let divStyle = {
      display: 'none',
  };
  
  if (props.isSuccess) {
    divStyle = {
      display: 'block',
    };
  }
  return (

    <div className="loader-overlay-spiner" style={divStyle}>
      <div className="loading-spiner">
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>  
  );
}

export default Spiner;
