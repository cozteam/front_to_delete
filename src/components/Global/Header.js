import React from 'react';
import { useDispatch, useSelector } from 'react-redux'
import {updateUser} from '../../store/actions/User'
import {baseURLFour} from '../../config';
import axios from "axios";
import { useHistory } from "react-router-dom";

function Header() {
  const userName = useSelector(state => state.UserReducers.username);
  const userID = useSelector(state => state.UserReducers.user_id);
  const dispatch = useDispatch();
  const history = useHistory();

  const logout = () => {
      axios.get(`${baseURLFour}/logout?user_id=${userID}`)
        .then((response) => {
          dispatch(updateUser(null)); 
          history.push("/auth") 
      });
     
  }


  return (
      <header className="cards-header">
        <div id="js-particle-field" className="particle-field"></div>
        <div id="js-icosa-field" className="icosa-field"></div>
        <div className="cards-header-content">
           <a href="/" className="cards-main-logo"></a>
           <nav className="cards-navigation">
              <p className="cards-nav-item visual-link cards-nav-person" >{userName}
              </p>
              <p 
                href="#" 
                className="cards-nav-item visual-link cards-nav-logout"
                onClick={() => logout()}
              >
                LOGOUT
              </p>
           </nav>
        </div>
     </header>
  );
}

export default Header;
