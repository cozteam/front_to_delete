import React, { Component } from 'react';
import API from "../../helpers/Interceptor";

class ExperimentCreation extends Component {

      constructor(props) {
         super(props);
         this.state = {
            mouseInfo: [],
            graphs: '',
            mouseStatistics: [],
            buttonName: 'activity_onset',
            isArchiveModal: false,
            isDeleteModal: false,
          }

         this.selectPhoto = this.selectPhoto.bind(this);
         this.selectMouse = this.selectMouse.bind(this);
         this.generateInfo = this.generateInfo.bind(this);
         this.deletMouse = this.deletMouse.bind(this);
      }  
      
      componentWillReceiveProps (props){
         this.selectMouse(props);
         
      }

      selectPhoto(graphsName) {
         API.get(`card/plots?card_id=${this.props.cardID}&plot_name=${graphsName}`)
           .then((response) => {
            this.setState({
               graphs:`http://${response.data.plot_path}`
            });
         });  
         this.setState({
            buttonName:graphsName
         });
      };

   
      selectMouse (props) {
         API.get(`/card/details?card_id=${props.cardID}`)
           .then((response) => {
            this.setState({
               mouseInfo:response.data[0],
               mouseStatistics:response.data[0].mouse_statistics,
            });
         });

         API.get(`card/plots?card_id=${props.cardID}&plot_name=activity_onset`)
           .then((response) => {
            this.setState({
               graphs:`http://${response.data.plot_path}`
            });
         });  
      }

      generateInfo () {
        const info = this.state.mouseStatistics.map(article => (
          <div className="archive-edit-row" key={article.text}>
            <span className="archive-edit-title">{article.text}</span>
            <span id="avg-speed" className="archive-edit-value distance-val">{article.value}</span>
          </div> 
        ));
         return (
          <React.Fragment>
            {info}
          </React.Fragment>
        )
      }
      
      deletMouse(){
         API.delete(`/card?card_id=${this.props.cardID}`)
           .then((response) => {
               this.props.selectCarts();
         });  
       
      }
      
      archiveMouse(){
         API.get(`card/archive?card_id=${this.props.cardID}`)
           .then((response) => {
               this.props.selectCarts();
         });  
      } 
      

      

      deleteModal (props) {
         let divStyle = {
            display: 'none',
         };

         if (this.state.isDeleteModal) {
            divStyle = {
               display: 'block',
            };
         }
         return (
          <React.Fragment>
            <div className="overlay" id="overlay" style={divStyle}>
               <div 
                  className="loader-overlay" 
                  style={divStyle}
                  onClick={() => ( this.setState({ isDeleteModal: false }) )}
               >
               <div className="popup loader lds-dual-ring"></div>
               </div>
               <div className="mouse-modal" style={divStyle}>
                  <div className="mouse-modal-text">Delete Mouse ID {this.props.cardID}?</div>
                  <div className="mouse-delet-bottom">
                     <div 
                        className="information-btn-little active"
                         onClick={() => ( 
                            this.setState({ isDeleteModal: false }), 
                            this.deletMouse() 
                         )}
                     >
                        Yes
                     </div>
                     <div 
                        className="information-btn-little"
                        onClick={() => ( this.setState({ isDeleteModal: false }) )}
                     >
                        No
                     </div>
                  </div>
               </div>
            </div>
          </React.Fragment>
        )
      }

      archiveModal (props) {
         let divStyle = {
            display: 'none',
         };

         if (this.state.isArchiveModal) {
            divStyle = {
               display: 'block',
            };
         }
         return (
          <React.Fragment>
            <div className="overlay" id="overlay" style={divStyle}>
               <div 
                  className="loader-overlay" 
                  style={divStyle}
                  onClick={() => ( this.setState({ isArchiveModal: false }) )}
               >
               <div className="popup loader lds-dual-ring"></div>
               </div>
               <div className="mouse-modal" style={divStyle}>
                  <div className="mouse-modal-text">Archive Mouse ID {this.props.cardID}?</div>
                  <div className="mouse-delet-bottom">
                     <div 
                        className="information-btn-little active"
                         onClick={() => ( 
                            this.setState({ isArchiveModal: false }), 
                            this.archiveMouse() 
                         )}
                     >
                        Yes
                     </div>
                     <div 
                        className="information-btn-little"
                        onClick={() => ( this.setState({ isArchiveModal: false }) )}
                     >
                        No
                     </div>
                  </div>
               </div>
            </div>
          </React.Fragment>
        )
      }


      render() {
         let divStyle = {
            display: 'none',
         };

         if (this.props.isActive) {
            
            divStyle = {
               display: 'block',
            };
         }
         return (
             <React.Fragment>
               <div id="overlay" className="overlay" style={divStyle}>
                  <div className="loader-overlay" >
                     <div className="popup loader lds-dual-ring"></div>
                  </div>
                  <div data-key="" className="card-details" style={divStyle}>
                     <div 
                        id="close-archive-btn" 
                        className="close-btn popup__close-btn"
                        onClick={() => {this.props.hideModal()}}
                     >
                     </div>

                     <form action="#" className="form-archive">
                        <div className="mouse-information-left">
                           <div className="mouse-information-bottom">
                              <div className="mouse-information-row">
                                 <div 
                                    href="#" 
                                    className={this.state.buttonName === 'activity_onset' ? 'mouse-information-btn active': 'mouse-information-btn'}  
                                    onClick={() => this.selectPhoto("activity_onset")}
                                 >
                                    Activity Onset
                                 </div>
                                 <div 
                                    href="#" 
                                    className="mouse-information-btn" 
                                    className={this.state.buttonName === 'actogram' ? 'mouse-information-btn active': 'mouse-information-btn'}
                                    onClick={() => this.selectPhoto("actogram")}
                                 >
                                    Actogram
                                 </div>
                              </div>
                              <div 
                                 href="#" 
                                 className="mouse-information-btn" 
                                 className={this.state.buttonName === 'bout_length_distribution' ? 'mouse-information-btn active': 'mouse-information-btn'}
                                 onClick={() => this.selectPhoto("bout_length_distribution")}
                              >
                                 Bout Length Distribution
                              </div>
                              <div className="mouse-information-row">
                                 <div 
                                    href="#" 
                                    className="mouse-information-btn" 
                                    className={this.state.buttonName === 'daily_bouts' ? 'mouse-information-btn active': 'mouse-information-btn'}
                                    onClick={() => this.selectPhoto("daily_bouts")}
                                 >
                                    Daily Bouts
                                 </div>
                                 <div 
                                    href="#" 
                                    className="mouse-information-btn"
                                    className={this.state.buttonName === 'relative_amplitude' ? 'mouse-information-btn active': 'mouse-information-btn'} 
                                    onClick={() => this.selectPhoto("relative_amplitude")}
                                 >
                                    Relative Amplitude
                                 </div>
                              </div>
                              <div 
                                 href="#" 
                                 className="mouse-information-btn" 
                                 className={this.state.buttonName === 'light_phase_activity' ? 'mouse-information-btn active': 'mouse-information-btn'} 
                                 onClick={() => this.selectPhoto("light_phase_activity")}
                              >
                                 Light Phase Activity
                              </div>
                              <div 
                                 href="#" 
                                 className="mouse-information-btn" 
                                 className={this.state.buttonName === 'intradaily_variability' ? 'mouse-information-btn active': 'mouse-information-btn'} 
                                 onClick={() => this.selectPhoto("intradaily_variability")}
                              >
                                 Intradaily Variability
                              </div>
                              <div 
                                 href="#" 
                                 className="mouse-information-btn" 
                                 className={this.state.buttonName === 'periodogram' ? 'mouse-information-btn active': 'mouse-information-btn'} 
                                 onClick={() => this.selectPhoto("periodogram")}
                              >
                                 Periodogram
                              </div>

                           </div>
                           <div className="archive-edit-block">
                              {this.generateInfo()}
                           </div>
                           <div className="archive-edit-block">
                              {this.mouseSetting}
                              <div className="archive-edit-row">
                                 <span className="archive-edit-title">Age, weeks</span>
                                 <span id="total-distance" className="archive-edit-value distance-val">{this.state.mouseInfo.age_in_weeks}</span>
                              </div>
                              <div className="archive-edit-row">
                                 <span className="archive-edit-title">Days remains</span>
                                 <span id="avg-speed" className="archive-edit-value distance-val">{this.state.mouseInfo.days_remains}</span>
                              </div>
                              <div className="archive-edit-row">
                                 <span className="archive-edit-title">Date of start</span>
                                 <span id="avg-speed" className="archive-edit-value distance-val">{this.state.mouseInfo.dos}</span>
                              </div>
                              <div className="archive-edit-row">
                                 <span className="archive-edit-title">Date of end</span>
                                 <span id="avg-speed" className="archive-edit-value distance-val">{this.state.mouseInfo.doe}</span>
                              </div>
                              <div className="archive-edit-row">
                                 <span className="archive-edit-title">Mouse ID</span>
                                 <span id="avg-speed" className="archive-edit-value distance-val">{this.state.mouseInfo.mouse_id}</span>
                              </div>
                              <div className="archive-edit-row">
                                 <span className="archive-edit-title">Sensor ID</span>
                                 <span id="avg-speed" className="archive-edit-value distance-val">{this.state.mouseInfo.sensor_id}</span>
                              </div>
                              <div className="archive-edit-row">
                                 <span className="archive-edit-title">Treatment</span>
                                 <span id="avg-speed" className="archive-edit-value distance-val">{this.state.mouseInfo.treatment}</span>
                              </div>
                              <div className="archive-edit-row">
                                 <span className="archive-edit-title">Genotype</span>
                                 <span id="avg-speed" className="archive-edit-value distance-val">{this.state.mouseInfo.genotype}</span>
                              </div>
                              <div className="archive-edit-row">
                                 <span className="archive-edit-title">Sex</span>
                                 <span id="avg-speed" className="archive-edit-value distance-val">{this.state.mouseInfo.sex}</span>
                              </div>     
                           </div>

                           <div className="archive-btn-wrap">
                              <div 
                                 className="submit-btn submit-archive-btn"
                                 onClick={() => (
                                       this.setState({ isArchiveModal: true }), 
                                       this.props.hideModal()
                                    )}
                              >
                                 ARCHIVE
                              </div> 
                              <div 
                                 className="submit-btn submit-delete-btn"
                                 onClick={() => (
                                       this.setState({ isDeleteModal: true }), 
                                       this.props.hideModal()
                                    )}
                              >
                                 DELETE
                              </div>
                           </div>
                           <div className="card_info">
                              <p className="card-details__card-id"></p>
                           </div>

                        </div>
                        <div className="mouse-information-right">
                           <img src={this.state.graphs} alt=""/>
                        </div>
                     </form>

                     <div className="portrait-warning">Please rotate your device to landscape orientation</div>
                  </div>
               </div>
               {this.deleteModal()}
               {this.archiveModal()}
            </React.Fragment>
         );
      }
}


export default ExperimentCreation;

