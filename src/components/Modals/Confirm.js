import React  from 'react';

	function Confirm(props) {

        let divStyle = {
            display: 'none',
        };
        
        if (props.isSuccess) {
            divStyle = {
                display: 'flex',
            };
        }
        return (
          <React.Fragment>
            <div 
              className="overlay" 
              style={divStyle}
            >
              <div 
                className="popup success_order_send" 
                style={divStyle}
              >
                <div 
                  className="popup__close-btn make-order__close"
                  onClick={() => {props.hideModal()}}
                >
                </div>
                <img className="success_img" src="/assets/base/img/checked.svg"/>
                <p className="success_text">{props.isSuccessText}</p>
              </div>
            </div>  
          </React.Fragment>
        );
   
}


export default Confirm;

