import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import API from "../../helpers/Interceptor";
import APITwo from "../../helpers/InterceptorTwo";
import { useSelector } from 'react-redux';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import format from "date-fns/format"


	function ExperimentCreation(props) {
    const [experimentName, setExperimentName] = useState("");
    const [computer, setComputer] = useState("");
    const [dateStart, setDateStart] = useState();
    const [dateEnd, setDateEnd] = useState();
    const [computers, setComputers] = useState([]);
    const userID = useSelector(state => state.UserReducers.user_id);
    const [date, setDate] = useState(new Date());

    useEffect(() => {
      selectComputers();
    }, []);

    const createCart = (props) => {
      API.post(`/experiment?experiment_name=${experimentName}&user_id=${userID}&dos=${dateConvertation(dateStart)}&doe=${dateConvertation(dateEnd)}&computer=[${computer}]`)
        .then((response) => {
          props.selectAllCarts();
          props.hideModal()
      });
    }

    const selectComputers = () => {
      APITwo.get(`get_computers_by_user_id?user_id=${userID}`)
      .then((response) => {
           setComputers(Array.from(response.data));
      });
    }


    const selectComputersasdsa = (data) => {
      console.log(data);
    }


    const generateOption = () => {
      const PC = computers.map(article => (
        <option 
          key={article} 
          name={article} 
          value={article}
        >
          {article}
        </option>
      ));
       return (
        <select 
          className="mouse-option" 
          name="computers" 
          value={computer}
          onChange={e=> setComputer(e.target.value)}
        > 
        <option>  </option> 
        {PC}
        </select>  
      )
    }

    const dateConvertation = (timestamp) =>{
      let d = new Date(timestamp),
      yyyy = d.getFullYear(),
      mm = ('0' + (d.getMonth() + 1)).slice(-2),  
      dd = ('0' + d.getDate()).slice(-2),         
      time = yyyy + '-' + mm + '-' + dd;
      return time;
    }


      let divStyle = {
        display: 'none',
      };
      
      if (props.isActive) {
        divStyle = {
            display: 'block',
        };
      }
    
      return (
        <React.Fragment>
            <div style={divStyle} className="add">
              <div 
                id="close-add-btn" 
                className="close-btn popup__close-btn"
                onClick={() => {props.hideModal()}}
              ></div>
              <form className="form-add">

                 <div className="form-el-wrap">
                    <label htmlFor="form_date-birth">Experiment name</label>
                    <input 
                      required 
                      type="text" 
                      className="dob-input date-picker"
                      value={experimentName}
                      onChange={e => setExperimentName(e.target.value)}
                    />
                 </div>
                 <div className="form-el-wrap">
                    <label htmlFor="form_date-start-analysis">Computer</label>
                      {generateOption()}
                 </div>
                 <div className="form-el-wrap">
                    <label htmlFor="form_date-stop-analysis">Date of start</label>
                    <DatePicker
                      selected={dateStart}
                      dateFormat="yyyy-MM-dd" 
                      onChange={setDateStart}
                    />
                 </div>
                 <div className="form-el-wrap">
                    <label htmlFor="form_treatment">Date of end</label>
                    <DatePicker
                      onChange={setDateEnd}
                      selected={dateEnd}
                      dateFormat="yyyy-MM-dd"
                    />
                 </div>

                 <div className="form-btn-wrap">
                    <div 
                      className="submit-btn add-card-btn"
                      onClick={() => {createCart(props)}}
                    >
                      CREATE
                    </div>
                 </div>
              </form>
          </div>

  
        <div 
          style={divStyle} 
          className="loader-overlay" 
          onClick={() => {props.hideModal()}}
        >
          <div className="popup loader lds-dual-ring"></div>
        </div>  
      </React.Fragment>

      );
    
}


export default ExperimentCreation;

