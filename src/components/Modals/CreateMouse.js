import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import API from "../../helpers/Interceptor";
import APITwo from "../../helpers/InterceptorTwo";
import { useForm } from 'react-hook-form';
import axios from 'axios';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useSelector } from 'react-redux';


	function CreateMouse(props) {
 
      const [computers, setComputers] = useState([]);
      const [maleChecked, setMaleChecked] = useState(true);
      const [sex, setSex] = useState("male");
      const [femaleChecked, setFemaleChecked] = useState(false);
      const {register, handleSubmit, errors} = useForm(); // initialise the hook
      const userID = useSelector(state => state.UserReducers.user_id);
      const [date, setDate] = useState();

      useEffect(() => {
        selectComputers();
      }, []);


      const selectComputers = () => {
        APITwo.get(`get_computers_by_user_id?user_id=${userID}`)
        .then((response) => {
             setComputers(Array.from(response.data));
        });
      }


      const generateOption = () => {
        const PC = computers.map(article => (
          <option key={article} >{article}</option>
        ));
         return (
          <select 
            className="mouse-option" 
            name="computers" 
            ref={register({ required: true })}
          > 
            <option> </option> 
            {PC}
          </select>  
        )
      }


      const onSubmit = data => {
        API.post(`/card?experiment_id=${props.experimentID}&computer_id=${data.computers}&mouse_id=${data.mouseID}&sensor_id=${data.sensorID}&sex=${sex}&treatment=${data.treatment}&genotype=${data.genotype}&dob=${dateConvertation(date)}`)
          .then((response) => {
            if (response.data.result === 'Error') {
              alert(response.data.msg);
            }
            else{
              props.selectCarts();
              props.hideModal();
            }
        });
        
      };

      const dateConvertation = (timestamp) =>{
        let d = new Date(timestamp),
        yyyy = d.getFullYear(),
        mm = ('0' + (d.getMonth() + 1)).slice(-2),  
        dd = ('0' + d.getDate()).slice(-2),         
        time = yyyy + '-' + mm + '-' + dd;
        return time;
      }

      const selectSex = sex => {
        if (sex === "Male") {
          setMaleChecked(true);
          setFemaleChecked(false);
          setSex("male");
        }
        else{
          setMaleChecked(false);
          setFemaleChecked(true);
          setSex("female");
        }     
      };


      let divStyle = {
       display: 'none',
      };

      if (props.isActive) {
       divStyle = {
           display: 'block',
       };
      }
        
      return (

        <React.Fragment>
          <div style={divStyle} className="add">
             <div 
                id="close-add-btn" 
                className="close-btn popup__close-btn"
                onClick={() => {props.hideModal()}}
             >
             </div>
              
              <form className="form-add" onSubmit={handleSubmit(onSubmit)}>
                <div className="top-form">
                   <div className="form-top-box">
                      <div className="form-top-title">Sensor ID</div>
                      <div className="form-el">
                         <input  
                            type="number" 
                            className="sensor_id" 
                            name="sensorID"
                            ref={register({ required: true })}
                         />
                      </div>
                   </div>
                   <div className="form-top-box">
                      <div className="form-top-title">Mouse ID</div>
                      <div className="form-el">
                         <input  
                             type="number" 
                             className="mouse_id"
                             name="mouseID" 
                             ref={register({ required: true })}
                         />
                      </div>
                   </div>
                </div>
                <div className="form-gender">
                   <div className="form-gender-item" onClick={() => {selectSex("Male")}}>
                      <input 
                        id="form_male" 
                        type="radio" 
                        name="sex" 
                        className="sex-input" 
                        defaultChecked ={ maleChecked }
                      />
                      <label htmlFor="form_male">
                      <span className="radio"></span>
                      <span className="label">Male</span>
                      </label>
                   </div>
                   <div className="form-gender-item" onClick={() => {selectSex("Female")}}>
                      <input 
                        id="form_female" 
                        type="radio" 
                        name="sex" 
                        className="sex-input" 
                        defaultChecked ={ femaleChecked }
                      />
                      <label htmlFor="form_female">
                      <span className="radio"></span>
                      <span className="label">Female</span>
                      </label>
                   </div>
                </div>
                <div className="form-el-wrap">
                   <label htmlFor="form_date-birth">Date of Birth</label>
    
                   <DatePicker
                      className="dob-input date-picker"
                      onChange={setDate}
                      selected={date}
                      ref={register({ required: true })}
                      dateFormat="yyyy-MM-dd"
                    />
                </div>

                <div className="form-el-wrap">
                    <label htmlFor="form_treatment">Treatment</label>
                    <input  
                      required 
                      type="text" 
                      className="treatment-input"
                      name="treatment" 
                      ref={register({ required: true })}
                   />
                </div>
                <div className="form-el-wrap">
                   <label htmlFor="form_genotype">Genotype</label>
                   <input  
                      required 
                      type="text" 
                      className="genotype-input"
                      name="genotype" 
                      ref={register({ required: true })}
                   />
                </div>
                <div className="form-el-wrap">
                   <label>Computer</label>
                   {generateOption()}
                 </div>
                 <div className="form-btn-wrap">
                    <input 
                      type="submit" 
                      value="CREATE" 
                      className="creacte-mouse add-card-btn "
                    />
                </div>
             </form>
          </div>
              
        <div 
          className="loader-overlay" 
          style={divStyle}
          onClick={() => {props.hideModal()}}
        >
        <div className="popup loader lds-dual-ring"></div>
        </div>  
      </React.Fragment>

        );
    
}



export default CreateMouse;

