import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import API from "../../helpers/InterceptorTwo";
import Confirm from '../../components/Modals/Confirm';


	function MakeOrder(props) {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [textarea, setTextarea] = useState("");
    const [errorName, setErrorName] = useState(false);
    const [errorEmail, setErrorEmail] = useState(false);
    const [isModalConfirm, setIsModalConfirm] = useState(false);
    

    const saveOrder = () => {
      (name.length >= 3) ? setErrorName(false) : setErrorName(true);
      (validateEmail(email)) ? setErrorEmail(false) : setErrorEmail(true);

      if (errorEmail === false && errorName === false) {
        API.post(`user_reguest?name=${name}&email=${email}&question=${textarea}`)
        .then((response) => {
          props.hideModal()
          setIsModalConfirm(true)
          setTimeout(function() {
              setIsModalConfirm(false)
          }, 3000);
          
        });
      }
    }

    const validateEmail = (email) => {
      var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        return re.test(String(email).toLowerCase());
    }

    const hideModalConfirm = (email) => {
      setIsModalConfirm(!isModalConfirm);
    }



        let divStyle = {
            display: 'none',
        };
        
        if (props.isActive) {
            divStyle = {
                display: 'block',
            };
        }

			return (
			  <React.Fragment>
          <div 
            class="overlay" 
            style={divStyle}
          >
            <div class="popup make-order-popup" style={divStyle}>
              <div 
                class="popup__close-btn make-order__close"
                onClick={() => {props.hideModal()}}
              >
              </div>
              <form autocomplete="off" class="popup__form make-order__form">
                  <input type="hidden" autocomplete="false"/>
                  <div class="form__field">
                      <p class="form__input-name">Name</p>
                      <input 
                        type="text" 
                        required 
                        class="form__input name-input" 
                        value={name}
                        onChange={e => setName(e.target.value)}
                        class={errorName == true ? 'form__input error-input': 'form__input'}
                      />
                      <div class={errorName == true ? 'error': 'close'}>
                        Required field
                      </div>
                  </div> 
                  <div class="form__field">
                      <p class="form__input-name">E-mail</p>
                      <input 
                        type="email" 
                        required 
                        class="form__input email-input"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        class={errorEmail == true ? 'form__input error-input': 'form__input'}
                      />
                      <div class={errorEmail == true ? 'error': 'close'}>
                        Required field
                      </div>
                  </div>
                  <div class="form__field">
                      <p class="form__input-name">Your question</p>
                      <textarea 
                        class="form__textarea text-input"
                        value={textarea}
                        onChange={e => setTextarea(e.target.value)}
                      >
                      </textarea>
                  </div>
                  <button type="submit" ></button>
                  <div 
                    class="form__send-btn order-send"
                    onClick={() => {saveOrder()}}
                  >
                  SEND</div>
              </form>
            </div>
          </div>	
         
          <Confirm
            isSuccess={isModalConfirm}
            hideModal = {hideModalConfirm}
            isSuccessText="We got your order! You will be contacted soon."
          />
         
			</React.Fragment>
        
			);
   
}


export default MakeOrder;

