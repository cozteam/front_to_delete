import React, { Component } from 'react';
import ReactDOM from 'react-dom';

	function AddCart(props) {

		const  SelectCarts = () => {
			
			return (

			<React.Fragment>
				<div className="add">
          <div id="close-add-btn" className="close-btn popup__close-btn"></div>
            <form className="form-add">
               <div className="top-form">
                  <div className="form-top-box">
                     <div className="form-top-title">Sensor ID</div>
                     <div className="form-el">
                        <input  required type="number" className="sensor_id" />
                     </div>
                  </div>
                  <div className="form-top-box">
                     <div className="form-top-title">Mouse ID</div>
                     <div className="form-el">
                        <input  required type="number" className="mouse_id"/>
                     </div>
                  </div>
               </div>
               <div className="form-gender">
                  <div className="form-gender-item">
                     <input id="form_male" type="radio" name="sex" className="sex-input" checked/>
                     <label htmlFor="form_male">
                     <span className="radio"></span>
                     <span className="label">Male</span>
                     </label>
                  </div>
                  <div className="form-gender-item">
                     <input id="form_female" type="radio" name="sex" className="sex-input"/>
                     <label htmlFor="form_female">
                     <span className="radio"></span>
                     <span className="label">Female</span>
                     </label>
                  </div>
               </div>
               <div className="form-el-wrap">
                  <label htmlFor="form_date-birth">Date of Birth</label>
                  <input required type="text" className="dob-input date-picker"/>
               </div>
               <div className="form-el-wrap">
                  <label htmlFor="form_date-start-analysis">Start Analysis</label>
                  <input required type="text" className="dos-input date-picker"/>
               </div>
               <div className="form-el-wrap">
                  <label htmlFor="form_date-stop-analysis">Stop Analysis</label>
                  <input required type="text" className="doe-input date-picker"/>
               </div>
               <div className="form-el-wrap">
                  <label htmlFor="form_treatment">Treatment</label>
                  <input  required type="text" className="treatment-input"/>
               </div>
               <div className="form-el-wrap">
                  <label htmlFor="form_genotype">Genotype</label>
                  <input  required type="text" className="genotype-input"/>
               </div>
               <div className="form-el-wrap">
                  <label htmlFor="form_experiment_namet">Experiment name</label>
                  <input  required type="text" className="experiment_name-input"/>
               </div>
               <div className="form-el-wrap">
                  <label htmlFor="form_cageId">Cage ID</label>
                  <input  required type="number" className="cage_id-input"/>
               </div>
               <div className="form-el-wrap">
                  <label>Computer</label>
                  <select required name="computer" className="computer-input"></select>
               </div>
               <div className="form-textarea-wrap">
                  <textarea  placeholder="Comments" className="comments-input"></textarea>
               </div>
               <div className="form-btn-wrap">
                  <div className="submit-btn add-card-btn">CREATE</div>
                  <button type="submit" ></button>
               </div>
            </form>
        </div>

        <div className="loader-overlay">
             <div className="popup loader lds-dual-ring"></div>
        </div>	
			</React.Fragment>

			);
		};


        return (
			<SelectCarts/>
    	);
    
}


export default AddCart;

