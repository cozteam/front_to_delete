import React from 'react';
import ReactDOM from 'react-dom';
import API from "../../helpers/Interceptor";
import {reportActions} from '../../store/actions/ReportActions'
import {printActions} from '../../store/actions/PrintActions'
import {useDispatch} from 'react-redux'
import FileDownload from 'js-file-download';
import {baseURL} from '../../config';

import {
  BrowserRouter as 
  Router,
  useHistory,
  Link,
} from "react-router-dom";


	function FileBlue(props) {

		const dispatch = useDispatch();
		const history = useHistory();

		const SelectComputers = (computers) => {
			let  computerStr = '';
			for(let namberComputer of computers){
				computerStr += `${namberComputer},`
			}
			computerStr.substring(0, computerStr.length - 1)
			return (
				computerStr
			);
		}

		const SelectOwner = (owners) => {
			let  ownersStr = '';
			for(let owner of owners){
				ownersStr += `${owner},`
			}
			ownersStr.substring(0, ownersStr.length - 1)
			return (
				ownersStr
			);
		}

		const delect = (experimentID) => {
			if (window.confirm(`Are you sure you want to delete the experiment?`)) {
				API.delete(`/experiment?experiment_id=${experimentID}`)
		          .then((response) => {
		            props.selectCarts();
		        });
			}	        
		}

		const archive = (experimentID) => {
			if (window.confirm(`Are you sure you want to transfer the experiment to the archive?`)) {
				API.get(`/experiment/archive?experiment_id=${experimentID}`)
		          .then((response) => {
		            props.selectCarts(); 
		        }); 
		        
	        }      
		}

		const share = (experimentID) => {
			
			API.get(`/experiment/share?experiment_id=${experimentID}`)
	          .then((response) => {
	            navigator.clipboard.writeText(response.data.share_link)
					.then(() => {
					    alert('Link saved to clipboard');
					})
					.catch(err => {
					alert('Something went wrong', err);
				});
	        });    
		}

		const download = (experimentID) => {
			API.get(`/experiment/download?experiment_id=1`)
	          .then((response) => {
	          	FileDownload(response.data, 'filename.csv');
	        });      
		}

		const report = (experimentID ) => {
			API.get(`/experiment/report?experiment_id=${experimentID}`)
	          .then((response) => {
	          	dispatch(reportActions("experiment")); 
	          	history.push("/report");  
	           	console.log(response);
	        });      
		}


		const print = (experimentID) => {
			API.get(`/experiment/print?experiment_id=${experimentID}`)
	          .then((response) => {
	          	dispatch(printActions("print"));

	           	console.log(response);
	        });      
		}



		const SelectCarts = (propsValue) => {
			let allCarts = Array.from(propsValue.carts);
			const fileBlue = allCarts.map(article => (
			  	<div key={article.experiment_id}  href="#" className="card file-blue">
					<div className="file-blue-inner">
						<div className="file-blue-left">
							<Link  
				  				to={`mouse-cards/${article.experiment_id}`} 
								className="card-left-information"
							>
								<div className="card-left-row">
									<div className="card-row-name">
										Experiment name
									</div>
									<div className="card-row-value">
										{article.experiment_name}
									</div>
								</div>

								<div className="card-left-row">
									<div className="card-row-name">
										Computer
									</div>
									<div className="card-row-value">
										{SelectComputers(article.computer)}
									</div>
								</div>

								<div className="card-left-row">
									<div className="card-row-name">
										Mouse amount
									</div>
									<div className="card-row-value">
										{article.number_of_cards}
									</div>
								</div>

								<div className="card-left-row">
									<div className="card-row-name">
										Owner
									</div>
									<div className="card-row-value">
										{SelectOwner(article.owner)}
									</div>
								</div>

								<div className="card-left-row">
									<div className="card-row-name">
										Date of start
									</div>
									<div className="card-row-value">
										{article.dos}
									</div>
								</div>

								<div className="card-left-row">
									<div className="card-row-name">
										Date of end
									</div>
									<div className="card-row-value">
										{article.doe}
									</div>
								</div>

							</Link>

						</div>
						<div className="file-blue-right">

						

							<div className="printer">	
								<a 
									href={`${baseURL}/experiment/print?experiment_id=${article.experiment_id}`}
									target="_blank"
									rel = "noopener noreferrer"
								>
									<div className="printer-text">PRINT</div>
								</a>	
							</div>	

							<div className="report">
								<a 
									href={`${baseURL}/experiment/report?experiment_id=${article.experiment_id}`}
									target="_blank"
									rel = "noopener noreferrer"
								>
									<div className="printer-report">Report</div>
								</a>	
							</div>


							<a 
								className="download"
								href={`${baseURL}/experiment/download?experiment_id=${article.experiment_id}`}
								target="_blank"
								rel = "noopener noreferrer"
							>
								<div  className="download">
									<span >Download</span>
								</div>
							</a>

							<div 
								className="share"
								onClick={() => {share(article.experiment_id)}}
							>
								<span>SHARE</span>
							</div> 
							<div 
								className="archive"
								onClick={() => {archive(article.experiment_id, propsValue)}}
							>
								<span>Archive</span>
							</div>
							<div 
								className="delete"  
								onClick={() => {delect(article.experiment_id, propsValue)}}
							>
								<span>Delete</span>
							</div>
						</div>

					</div>
				</div>	
			));	
	
			return (
				<React.Fragment>
					{fileBlue}
				</React.Fragment>
			);

		};


	
        return (
			<SelectCarts  
				carts={props.carts}
				selectCarts={props.selectCarts}
				storeActions={props.storeActions}
			/>
    	);
    
}


export default FileBlue;

