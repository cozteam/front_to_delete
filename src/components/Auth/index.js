import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import API from "../../helpers/Interceptor";
import {updateUser, selectUser} from '../../store/actions/UserActions'
import {userData} from "../../store/tokenStorage";
import {withRouter, Link} from 'react-router-dom';
import {createStore} from 'redux'
import reducers from '../../store/reducers'
let store = createStore(reducers)


class Auth extends Component {
	
	constructor (props) {
	  super(props);
	  this.state = {
	    email: '',
	    password: '',
	    error: '',
	    emailValid: "#d2d2d2",
	    passwordValid: "#d2d2d2",
	    heightBlock:"auto",
	  }

		this.onEmailChange = this.onEmailChange.bind(this);
		this.onPasswordChange = this.onPasswordChange.bind(this);
		this.handleChange = this.handleChange.bind(this);
	  	this.scaleElevation = this.scaleElevation.bind(this);
	  
	}

	componentDidMount(){
		this.scaleElevation();
		this.confirmUser();
	}
	

	confirmUser(){
		console.log("mainBlock4441111");
		let token = this.props.match.params.token;
		if (token) {
			API.post('/auth/confirm-account', {
	            token: token,
	        })
	        .then((response) => {  
	       		if (!response.data.error) {
	       			console.log('response.data1');
	        		this.setState({  
	        			error: response.data.Massage,
	        		});
	        	}
	        	else{
	        		console.log('response.data2');
	        		let user = {
	        			CsrfToken: response.data.CsrfToken,
	        			userID: response.data.userID,
	        			balance: response.data.balance,
	        		};
	        		store.dispatch(updateUser(JSON.stringify(user)));
	        		this.props.history.push("/");
	        	}
            });

		}
	}

	scaleElevation(){
		let mainBlock = window.innerHeight; // вычисляем высоту главного контейнера
		this.setState({
			heightBlock: `${mainBlock}px`
		});
	}	

    onEmailChange(e) {
        let val = e.target.value;
        this.setState({email: val});
    }

    onPasswordChange(e)  {
        let val = e.target.value;
        this.setState({password: val});
    }

    handleChange() {

        let email = this.state.email;
        let password = this.state.password;

        /.+@.+\.[A-Za-z]+$/.test(email) ? this.setState({emailValid: "#d2d2d2"}) : this.setState({emailValid: "red"});
		password.length ? this.setState({passwordValid: "#d2d2d2"}) : this.setState({passwordValid: "red"}); 	

		if(this.state.emailValid !== "red" && this.state.passwordValid !== "red"){
	        API.post('/auth/authentication', {
	            email: email,
	            password: password
	        })
	        .then((response) => {  
	        	if (response.data.error === "true") {
	        		this.setState({ 
						emailValid: "red",
						passwordValid: "red",  
	        			error: response.data.Massage,
	        		});
	        	}
	        	else{
	        		let user = {
	        			CsrfToken: response.data.CsrfToken,
	        			userID: response.data.userID,
	        			balance: response.data.balance,
	        		};
	        		store.dispatch(updateUser(JSON.stringify(user)));
	        		this.props.history.push("/");
	        	}
            });
	    }    	
    }


    render() {
        return (
            <div>
				<div className="container-fluid">
					<div className="row">
						<div className="right-column sisu">
							<div className="row mx-0">
								<div className="col-md-7 order-md-2 signin-right-column px-5 bg-dark">
									<a className="signin-logo d-sm-block d-md-none" href="#">
										<img src="assets/img/logo.png" width="145" height="32.3" alt="QuillPro" />
									</a>
									<h2 className="display-5">"Сайт еще не готов. Не пытайтесь регистрироваться."</h2>
									<p className="lead mb-5">
										Админ.
									</p>
								</div>
								<div className="col-md-5 order-md-1 signin-left-column bg-white px-5" style={{height:this.state.heightBlock}} >
									<a className="signin-logo d-sm-none d-md-block" href="#">
										<img src="./assets/img/logo.png" width="145" height="32.3" alt="logo" />
									</a>
									<form >
										<div className="form-group">
											<div className="error">{this.state.error}</div>
											<label htmlFor="exampleInputEmail1">Email</label>
											<input  type="email" 
													className="form-control" 
													id="exampleInputEmail1" 
													aria-describedby="emailHelp" 
													placeholder="Введите email"  
													value={this.state.email} 
													onChange={this.onEmailChange} 
													style={{borderColor:this.state.emailValid}} />
											<small id="emailHelp" className="form-text text-muted"></small>
										</div>
										<div className="form-group">
											<label htmlFor="exampleInputPassword1">Пароль</label>
											<input  type="password" className="form-control" 
													id="exampleInputPassword1" 
													placeholder="Введите  пароль" 
													value={this.state.password}  
													onChange={this.onPasswordChange}  
													style={{borderColor:this.state.passwordValid}}/>
										</div>
										<div className="custom-control custom-checkbox mb-3">
											<input type="checkbox" 
											className="custom-control-input" id="keep-signed-in"/>
											{/*<label className="custom-control-label" for="keep-signed-in">Запомнить меня</label>*/}
										</div>
										<div 	type="submit" 
												className="btn btn-primary btn-gradient btn-block" 
												onClick={this.handleChange}>
											<i className="batch-icon batch-icon-key">Войти</i>
										</div>
										<hr></hr>
										<p className="text-center">
											У Вас нет аккаунта? <Link to="/registration">Зарегистрировать</Link>
										</p>
										<p className="text-center">
											Вы не помните пароль? <Link to="/forgot-password">Сброс пароля</Link>
										</p>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        );
    }
}

export default Auth;
