import React from "react";
import Slider from "react-slick";


class MainSlider extends React.Component {
  render() {
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <Slider {...settings}>
         <div class="carousel-item">
		    <img controls="controls" alt="" src="/assets/media/cage_1_moLxv84.jpeg"/>
		</div>

		<div class="carousel-item">
		    <img controls="controls" alt="" src="/assets/media/cage_2_ZUeXzhv.jpeg"/>
		</div>

		<div class="carousel-item">
		    <img controls="controls" alt="" src="/assets/media/cage_4_ugs0bOB.jpeg"/>
		</div>
      </Slider>
    );
  }
}

export default (MainSlider);