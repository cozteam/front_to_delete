import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import API from "../../helpers/Interceptor";
import {reportActions} from '../../store/actions/ReportActions'
import MouseInformation from '../../components/Modals/MouseInformation';

		
	function FileBlue(props) {
		const [isMouseInformation, setIsMouseInformation] = useState(false);
		const [cardID, setCardID] = useState('');

		const SelectComputers = (computers) => {
			let  computerStr = '';
			for(let namberComputer of computers){
				computerStr += `${namberComputer},`
			}
			computerStr.substring(0, computerStr.length - 1)
			return (
				computerStr
			);
		}

		const SelectOwner = (owners) => {
			let  ownersStr = '';
			for(let owner of owners){
				ownersStr += `${owner},`
			}
			ownersStr.substring(0, ownersStr.length - 1)
			return (
				ownersStr
			);
		}

		const delect = (experimentID, props) => {
			if (window.confirm(`Are you sure you want to delete the experiment?`)) {
				API.delete(`/experiment?experiment_id=${experimentID}`)
		          .then((response) => {
		            console.log(response);
		        });
		  		props.selectCarts();
			}	        
		}

		const archive = (experimentID, props) => {
			if (window.confirm(`Are you sure you want to transfer the experiment to the archive?`)) {
				API.get(`/experiment/archive?experiment_id=${experimentID}`)
		          .then((response) => {
		            console.log(response);
		        }); 
		        props.selectCarts(); 
	        }      
		}

		const share = (experimentID) => {
			API.get(`/experiment/share?experiment_id=${experimentID}`)
	          .then((response) => {
	            navigator.clipboard.writeText(response.data.share_link)
					.then(() => {
					    alert('Link saved to clipboard');
					})
					.catch(err => {
					console.log('Something went wrong', err);
				});
	        });    
		}

		const download = (experimentID) => {
			API.get(`/experiment/download?experiment_id=${experimentID}`)
	          .then((response) => {
	            console.log(response);
	        });      
		}

		const report = (experimentID ) => {
			API.get(`/experiment/report?experiment_id=${experimentID}`)
	          .then((response) => {
	           	console.log(response);
	        });      
		}


		const print = (experimentID) => {
			API.get(`/experiment/print?experiment_id=${experimentID}`)
	          .then((response) => {
	           	console.log(response);
	        });      
		}



	    const openMouseInformation = (CardID) => {
	    	setCardID(CardID);
	        setIsMouseInformation(true);
	    }


	    const hideModalInformation = () => {
	        setIsMouseInformation(false);
	    }


		const SelectCarts = (propsValue , store) => {
			let allCarts = Array.from(propsValue.carts);
			const fileBlue = allCarts.map(article => (
			 	<div 
				 	className="card-mouse" 
				 	key={article.card_id} 
				 	onClick={() => {openMouseInformation(article.card_id)}}
			 	>
                  <div className="card-top">
                     <div className="card-header">
                        <div className="card-top-box">
                           <div className="card-top-title">Sensor ID</div>
                           <div className="card-sensorId">{article.card_id}</div>
                        </div>
                        <div className="card-top-box">
                           <div className="card-top-title">Mouse ID</div>
                           <div className="card-mouseId">{article.mouse_id}</div>
                        </div>
                     </div>
                  </div>

	                <div className="card-bottom">
	                  	<div className="card-bottom-row">
	                        <span className="card-bottom-title">Mouse age</span>
	                        <span className="card-bottom-value">{article.age_in_weeks}</span>
	                    </div>
	                    <div className="card-bottom-row">
	                        <span className="card-bottom-title">Sex</span>
	                        <span className="card-bottom-value">{article.sex}</span>
	                    </div>
						<div className="card-bottom-row">
	                        <span className="card-bottom-title">Treatment</span>
	                        <span className="card-bottom-value">{article.treatment}</span>
	                    </div>
	                    <div className="card-bottom-row">
	                        <span className="card-bottom-title">Genotype</span>
	                        <span className="card-bottom-value">{article.genotype}</span>
	                    </div>
	                    <div className="card-bottom-row">
	                        <span className="card-bottom-title">Computer</span>
	                        <span className="card-bottom-value">{article.computer_id}</span>
	                    </div>
	                </div>
               </div>	
			));	
	



			return (
				<React.Fragment>
					{fileBlue}
				</React.Fragment>
			);

		};




        return (
        	<React.Fragment>
	        	<MouseInformation
	              isActive={isMouseInformation}
	              hideModal={hideModalInformation}
	              cardID={cardID}
	              selectCarts={props.selectCarts}
	            />
				<SelectCarts  
					carts={props.carts}
				/>
			</React.Fragment>	
    	);
    
}


export default FileBlue;

