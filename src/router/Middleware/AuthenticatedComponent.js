import React from 'react'
import { connect } from 'react-redux'
import { useHistory } from "react-router-dom";


export default  function requireAuthentication(Component) {

  class AuthenticatedComponent extends React.Component {
    render() {
      return <React.Fragment>{this.props.user !== '' ? <Component {...this.props} /> : window.location.replace("/auth") }</React.Fragment>
    }
  }

  function mapStateToProps(state) {
    return {
      user: state.UserReducers
    }
  }

  return connect(mapStateToProps)(AuthenticatedComponent)
}