$(document).ready(function () {



  $(".add-card").on("click", function () { //show add card window
    $("#overlay").fadeIn(300).addClass("active");
    $('.add-user').fadeIn(300).addClass("active");
    return false;
  });


  $('.add-user-btn').click(function () {
    let btn = $(this);
    let form = btn.parent().parent();

    if (form[0].checkValidity()) {
      let data_for_send = new FormData();
      //fields in 'data_for_send' should be named like in UserSerializer
      data_for_send.append('email', form.find('#input__employer-email').val());
      data_for_send.append('username', form.find('#input__employer-username').val());
      let parent = $(this).parent().parent().parent();
      $(".loader").fadeIn(300).addClass("active");
      $(".loader-overlay").fadeIn(300).addClass("active");
      $('.form-add input').val('');
      axios_instance.post(ADD_URL, data_for_send, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function (response) {
        if (response.status === 200) {
          $('.add-card').before(response.data);
          $('.employees-count').text(Number($('.employees-count').text()) + 1);
        } else {
          alert(SERVER_ERR);
          console.log(error);
        }
        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'), parent);
      })
        .catch(function (error) {
          if (error.response.status === 400) {
            let field = error.response.data['field'];
            let code = error.response.data['code'];
            let error_class = ['.error',field, code].join('-');
            alert($(error_class).text());
            remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'), parent);
          } else {
            alert(SERVER_ERR);
            console.log(error);
            remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'), parent);
          }
        });
    } else {
      $(this).parent().find('button[type=submit]').click()
    }
  });





  $('body').on('click', '.delete-user-btn', function () {
    $('.overlay').fadeIn(300).addClass("active");
    $('.delete-submit-popup').fadeIn(300).addClass("active").attr('data-key', $(this).parent().parent().attr('data-key'));
  });






  //confirm delete window
  $('.delete-submit-popup').on('click', '.yes-btn', function () {
    $(".loader").fadeIn(300).addClass("active");
    $(".loader-overlay").fadeIn(300).addClass("active");
    remove_popup($('.delete-submit-popup'));
    delete_user($('.delete-submit-popup').attr('data-key'))

  });

  $('.delete-submit-popup').on('click', '.no-btn', function () {
    remove_popup($('.delete-submit-popup'), $('.overlay'));


  });


  function delete_user(user_id) {
    $(".loader").fadeIn(300).addClass("active");
    $(".loader-overlay").fadeIn(300).addClass("active");
    axios_instance.delete(CHANGE_URL + user_id).then(function (response) {
      if (response.status === 204) {
        $('.card[data-key=' + user_id + ']').remove();
        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
        $('.employees-count').text(Number($('.employees-count').text()) - 1)
      }
      else {
        console.log(response.data);
        alert(SERVER_ERR);
        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
      }
    })
      .catch(function (error) {
        console.log(error);
        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
        alert(SERVER_ERR);
      });

  }

  $('body').on('click', '.reset-user-btn', function () {
    $('.overlay').fadeIn(300).addClass("active");
    $('.reset-submit-popup').fadeIn(300).addClass("active").attr('data-key', $(this).parent().parent().attr('data-key'));
  });




  //confirm reset window
  $('.reset-submit-popup').on('click', '.yes-btn', function () {
    $(".loader").fadeIn(300).addClass("active");
    $(".loader-overlay").fadeIn(300).addClass("active");
    remove_popup($('.reset-submit-popup'));
    reset_user($('.reset-submit-popup').attr('data-key'))
  });

  $('.reset-submit-popup').on('click', '.no-btn', function () {
    remove_popup($('.reset-submit-popup'), $('.overlay'));


  });


  function reset_user(user_id) {
    $(".loader").fadeIn(300).addClass("active");
    $(".loader-overlay").fadeIn(300).addClass("active");

    axios_instance.patch(CHANGE_URL + user_id + '/').then(function (response) {
      if (response.status === 206) {
        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
      }
      else {
        console.log(response.data);
        alert(SERVER_ERR);
        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
      }
    })
      .catch(function (error) {
        console.log(error);
        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
        alert(SERVER_ERR);
      });

  }

});


