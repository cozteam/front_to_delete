function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

const CSRF = getCookie('csrftoken');
const axios_instance = axios.create(
  {
    baseURL: window.location.origin,
    headers: { 'X-CSRFToken': CSRF },
  }
);

$("body").on("click", '.popup__close-btn', function () { //show add card window
  let parent = $(this).parent();
  $('.overlay').fadeOut(300);
  parent.fadeOut(300);

  setTimeout(function () {
    $('.overlay').removeClass('active');
    parent.removeClass('active').removeClass('active-flex');
    parent.find('input').val('');
    parent.find('textarea').val('');
  }, 300)
});


$("body").on("click", '.cards-nav-print', function () { //show add card window
  $('html').css('font-size', '7px').css('width', '800px');
  $('header').css('display', 'none');
  setTimeout(function () {
    window.print();
    $('html').css('font-size', '').css('width', '');
    $('header').css('display', '');
  }, 1)

});

$("body").on("click", '.connections-count', function () { //show add card window
  let btn = $(this)
  axios_instance.get(connections_url).then(function (response) {

    if (response.status === 200) {
      let connections = response.data.connections;
      btn.find('span').text(connections)
      btn.addClass('updated');
      setTimeout(function () {
        btn.removeClass('updated');
      }, 250)
    }
    else {
      console.log(response.data);
      alert(SERVER_ERROR_MSG);
    }

  })
    .catch(function (error) {
      console.log(error);
      alert(SERVER_ERROR_MSG);
    });

});

function remove_popup(...elems) {


  elems.forEach(function (elem) {
    console.log(elem);
    elem.fadeOut(300);
    setTimeout(function () {
      elem.removeClass('active').removeClass('active-flex');
    }, 300);
  });
}