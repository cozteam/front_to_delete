


$(".main-video-carousel").slick();

//--- cost of wheels part ---
let wheelCounter = +$("#wheel-counter").text();


$(".counter-decrease").on("click", function () { //count up
  wheelCounter--;
  if (wheelCounter <= 1) {
    wheelCounter = 1
  }
  $("#wheel-counter").text(wheelCounter);
  $(".card-price").html(wheelCounter * 15 + "<span>$</span>");
});
$(".counter-increase").on("click", function () { //count down
  wheelCounter++;
  $("#wheel-counter").text(wheelCounter);
  $(".card-price").html(wheelCounter * 15 + "<span>$</span>");
});

//--- end of cost of wheels part ---


//--- change active pay method part ---
let pay_method = 'mounthly'; //need for send form

$(".card-button").on("click", function () { //toggle cards
  if ($(this).hasClass("active")) {
    return false
  }
  $(".card-button.active").removeClass("active");
  $(this).addClass("active");
  if ($('.card-button.active').index() === 1) {
    pay_method = 'ежегодно';
  } else {
    pay_method = 'ежемесячно';
  }
});

//--- end of change active pay method part ---

$('.popup__close-btn').click(function () {
  let parent = $(this).parent();
  $('.overlay').fadeOut(300);
  parent.fadeOut(300);
  setTimeout(function () {
    $('.overlay').removeClass('active');
    parent.removeClass('active').removeClass('active-flex');
  }, 300)
});


//--- send order part ---
$('.order-btn').click(function () {
  $('.overlay').fadeIn(300).addClass('active');
  $('.make-order-popup').fadeIn(300).addClass('active');
});


$('.order-send').click(function () {

  let btn = $(this);
  if ($(this).parent()[0].checkValidity()) {
    let data_for_send = new FormData();

    //fields in 'data_for_send' should be named like in OrderSerializer
    data_for_send.append('name', btn.parent().find('.name-input').val());
    data_for_send.append('phone', btn.parent().find('.phone-input').val());
    data_for_send.append('email', btn.parent().find('.email-input').val());
    data_for_send.append('text', btn.parent().find('.text-input').val());
    data_for_send.append('wheels_count', $('.counter-number').text());
    data_for_send.append('pay_method', pay_method);
    $('.make-order-popup').fadeOut(300);
    setTimeout(function () {
      $('.make-order-popup').removeClass('active');
      $('.success_order_send').fadeIn(300).addClass('active-flex')
    }, 300);


    axios_instance.post("new_order/", data_for_send, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }).then(function (response) {
      let response_result = response.data.result;
      if (response_result === 'OK') {
        console.log('all_good')
      } else if (response_result === 'SERIALIZER_ERROR') {
        alert(SERVER_ERROR_MSG);
        console.log(response.data)
      }
    })
      .catch(function (error) {
        alert(SERVER_ERROR_MSG);
        console.log(error);
      });
  } else {
    $(this).parent().find('button[type=submit]').click()
  }
})
//--- end of send order part ---

//--- login part ---

$('.nav-person.open-popup').click(function () {
  $('.overlay').fadeIn(300).addClass('active');
  $('.login-popup').fadeIn(300).addClass('active');
});

function login_user(elem) {
  if ($(elem).val().length >= 6) {
    let btn = $(elem).parent();
    if (btn.parent()[0].checkValidity()) {
      let data_for_send = new FormData();

      //fields in 'data_for_send' should be named like in UserSerializer
      data_for_send.append('username', btn.parent().find('.login-input').val());
      data_for_send.append('password', btn.parent().find('.password-input').val());

      axios_instance.post(LOGIN_URL, data_for_send, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function (response) {
        console.log(response.data);
        let response_result = response.data.result;
        if (response_result === 'OK') {
          window.location.pathname = response.data.url
        } else if (response_result === 'WRONG_DATA') {
          alert($('.wrong-username-password').text())
        } else {
          alert(SERVER_ERROR_MSG)
        }
      })
        .catch(function (response) {
          console.log(response);
          alert(SERVER_ERROR_MSG)
        });
    } else {
      btn.parent().find('button[type=submit]').click()
    }
  }


}

//--- end of login part ---
