
$(document).ready(function () {

  function controll_dash_btns(days, hours) {
    $('.arc-head-time-btn').click(function () {
      if ($(this).hasClass('active')) {
        return false;
      }
      $('.arc-head-time-btn.active').removeClass('active');
      $(this).addClass('active');
      if ($(this).attr('data-time') === 'days') {
        $(days).click();
      } else {
        $(hours).click();
      }
    })
  }


  $("#add-card").on("click", function () { //show add card window
    $("#overlay").fadeIn(300).addClass("active");
    $('.add').fadeIn(300).addClass("active");
    return false;
  });

  $(".cards-archive-wrap").on("click", ".card", function () { //show card-details card window
    update_card_details($(this));
    return false;
  });
  $(".cards-active-wrap").on("click", ".card", function () { //show card-details card window
    update_card_details($(this));
    return false;
  });


  //remove old card details div and add a new one to overlay
  function update_card_details(card) {
    $("#overlay").fadeIn(300).addClass("active");
    axios_instance.get(CARD_DETAIL_URL + card.attr('data-key')).then(function (response) {

      if (response.status === 200) {
        $('.card-details').remove();
        $('#overlay').append(response.data);
        $('.card-details').fadeIn(300).addClass("active");
        let load_dash = setInterval(function () {
          if ($('iframe').contents().find("#agg-dropdown input:checked").parent().text() === '') {
          } else {
            clearInterval(load_dash);
            let dash_radio = $('iframe').contents().find("#agg-dropdown label");
            controll_dash_btns(dash_radio[0], dash_radio[1])
          }
        }, 1000)

      }
      else {
        alert(SERVER_ERROR_MSG);
        console.log(error);
        $('.overlay').fadeOut(300);
        setTimeout(function () {
          $('.overlay').removeClass('active');
        }, 300)
      }
    })
      .catch(function (error) {
        console.log(error);
        alert(SERVER_ERROR_MSG);
        $('.overlay').fadeOut(300);
        setTimeout(function () {
          $('.overlay').removeClass('active');
        }, 300)
      });

  }


  //add new card
  $('.add-card-btn').click(function () {

    let btn = $(this);
    let form = btn.parent().parent();
    console.log(form.find('.computer-input').val());
    if (form[0].checkValidity()) {
      let data_for_send = new FormData();
      //fields in 'data_for_send' should be named like in CardsSerializer
      data_for_send.append('sensor_id', form.find('.sensor_id').val());
      data_for_send.append('mouse_id', form.find('.mouse_id').val());
      data_for_send.append('sex', ($('.sex-input:checked').next().find('.label').text()).toString());
      data_for_send.append('dob', form.find('.dob-input').val());
      data_for_send.append('dos', form.find('.dos-input').val());
      data_for_send.append('doe', form.find('.doe-input').val());
      data_for_send.append('genotype', form.find('.genotype-input').val());
      data_for_send.append('treatment', form.find('.treatment-input').val());
      data_for_send.append('experiment_name', form.find('.experiment_name-input').val());
      data_for_send.append('cage_id', form.find('.cage_id-input').val());
      data_for_send.append('comments', form.find('.comments-input').val());
      data_for_send.append('computer', form.find('.computer-input').val());
      let parent = $(this).parent().parent().parent();
      $('.form-add input').val('');
      $('.form-add textarea').val('');

      axios_instance.post(ADD_URL, data_for_send, {
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(function (response) {

        if (response.status === 201) {
          $('#add-card').before(response.data)
        } else {
          console.log(response.data);
          alert(SERVER_ERROR_MSG)
          console.log(error);
        }
        remove_popup(parent, $('.overlay'));

      })
        .catch(function (error) {
          if (error.response.status === 403) {
            let field = error.response.data['field'];
            let code = error.response.data['code'];
            let error_class = ['.error', field, code].join('-');
            alert($(error_class).text());
          } else if (error.response.status === 409) {
            let field = error.response.data['field'];
            let code = error.response.data['code'];
            let error_class = ['.error', field, code].join('-');
            alert($(error_class).text());
          } else if (error.response.status === 406) {
            alert(COMPANY_ERROR_MSG)
          } else {
            alert(SERVER_ERROR_MSG);
            console.log(error);
          }
          remove_popup(parent, $('.overlay'));
        });
    } else {
      $(this).parent().find('button[type=submit]').click()
    }
  });


  //archive card

  //open confirm archiving window
  $('body').on('click', '.submit-archive-btn', function () {
    remove_popup($('.card-details'));
    $('.archive-submit-popup').fadeIn(300).addClass("active");

  });

  //confirm archiving window
  $('.archive-submit-popup').on('click', '.yes-btn', function () {
    $(".loader").fadeIn(300).addClass("active");
    $(".loader-overlay").fadeIn(300).addClass("active");
    remove_popup($('.archive-submit-popup'));
    archive_card($('.card-details').attr('data-key'))

  });

  $('.archive-submit-popup').on('click', '.no-btn', function () {
    remove_popup($('.overlay'), $('.archive-submit-popup'));
  });

  //send archiving request and archive card
  function archive_card(card_id) {
    let data_for_send = new FormData();
    data_for_send.append('status', 'archive');
    axios_instance.patch(CARD_DETAIL_URL + card_id + '/', data_for_send, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(function (response) {

      if (response.status === 200) {
        $('.card[data-key=' + card_id + ']').remove();
        $('.archive-box').append(response.data);
        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
      }
      else {
        console.log(response.data);
        alert(SERVER_ERROR_MSG);
        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
      }


    })
      .catch(function (error) {
        console.log(error);
        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
        alert(SERVER_ERROR_MSG);
      });

  }


  //change graphs

  $("body").on("click", '.arc-head-type-btn', function () {
    let btn = $(this);
    if (btn.hasClass("active")) {
      return false
    }
    if (btn.attr('data-type') === 'circadian') {
      $(".arc-head-time-wrap").hide()
    }
    //send request to api and add an image to archive-img-wrap + set it active
    if (btn.attr('data-type') === 'circadian' && btn.attr('data-toggle') === 'no') {
      $(".loader").fadeIn(300).addClass("active");
      $(".loader-overlay").fadeIn(300).addClass("active");
      axios_instance.get(ACTOGRAMM_URL + $('.card-details').attr('data-key')).then(function (response) {

        remove_popup($(".loader"), $(".loader-overlay"));

        if (response.status === 200) {

          $('.archive-img-wrap').append(response.data);
          $(".arc-head-type-btn.active").removeClass("active");
          btn.attr('data-toggle', 'yes');
          btn.addClass("active");
          changeImgGraph();
        }
        else {
          console.log(response.data);
          alert(SERVER_ERROR_MSG);
          remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
        }

      })
        .catch(function (error) {
          console.log(error);
          alert(SERVER_ERROR_MSG);
          remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
        });


    } else {
      $(".arc-head-type-btn.active").removeClass("active");
      btn.addClass("active");

      changeImgGraph();
    }


    return false;
  });

  // change image graph
  function changeImgGraph() {
    let img = ".archive-img-" + $(".arc-head-type-btn.active").attr("data-type");
    $(".archive-img").removeClass("active");
    $(img).addClass("active");
  }


  // datepickers in add card form
  let dateFormat = "yy-mm-dd";

  function getDate(element) {
    let date;
    try {
      date = $.datepicker.parseDate(dateFormat, element.value);
    } catch (error) {
      date = null;
    }

    return date;
  }

  $('.date-picker').keydown(function (e) {
    e.preventDefault();
  });

  $(".dob-input").datepicker({
    dateFormat: dateFormat,
    maxDate: new Date()
  });
  let from = $(".dos-input").datepicker({
    dateFormat: dateFormat,

  }).on('change', function () {
    to.datepicker("option", "minDate", getDate(this));
  });
  let to = $(".doe-input").datepicker({
    dateFormat: dateFormat,
  }).on('change', function () {
    from.datepicker("option", "maxDate", getDate(this));
  });

  // download 
  $("body").on("click", '.cards-nav-download', function () {
    $('.overlay').fadeIn(300).addClass("active");
    $('.experiment-download-select-popup').fadeIn(300);
  });

  $('.experiment-download-select-popup').on('click', '.yes-btn', function () {
    var exp_name = $('.experiment-download-select-popup select').val()
    console.log(exp_name)
    var link = document.createElement("a");
    link.download = "report_mats_" + exp_name + ".csv";
    link.href = DOWNLOAD_URL + exp_name;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    delete link;
    remove_popup($('.experiment-download-select-popup'), $('.overlay'));
  });

  $('.experiment-download-select-popup').on('click', '.no-btn', function () {
    remove_popup($('.experiment-download-select-popup'), $('.overlay'));
  });

  //delete card

  //open delete archiving window
  $('body').on('click', '.submit-delete-btn', function () {
    $('.card-details').fadeOut(300);
    setTimeout(function () {
      $('.card-details').removeClass('active').removeClass('active-flex');
    }, 300);
    $('.delete-submit-popup').fadeIn(300).addClass("active");

  });

  //confirm delete window
  $('.delete-submit-popup').on('click', '.yes-btn', function () {
    $(".loader").fadeIn(300).addClass("active");
    $(".loader-overlay").fadeIn(300).addClass("active");
    remove_popup($('.delete-submit-popup'));
    delete_card($('.card-details').attr('data-key'))

  });

  $('.delete-submit-popup').on('click', '.no-btn', function () {
    remove_popup($('.delete-submit-popup'), $('.overlay'));


  });

  //send delete request and delete card
  function delete_card(card_id) {
    axios_instance.delete(CARD_DETAIL_URL + card_id).then(function (response) {

      if (response.status === 204) {
        $('.card[data-key=' + card_id + ']').remove();
        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
      }
      else {
        console.log(response.data);
        alert(SERVER_ERROR_MSG);
        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
      }


    })
      .catch(function (error) {
        console.log(error);

        remove_popup($(".loader"), $(".loader-overlay"), $('.overlay'));
        alert(SERVER_ERROR_MSG);
      });

  }


  // send request for update comment and doe fields
  function update_card(form) {
    console.log(form.find('.doe-edit').val(), form.find('.comments-val').val())
    let data_for_send = new FormData();
    data_for_send.append('comments', form.find('.comments-val').val());
    data_for_send.append('doe', form.find('.doe-edit').val());
    axios_instance.patch(CARD_DETAIL_URL + $('.card-details').attr('data-key') + '/', data_for_send, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(function (response) {

      if (response.status === 200) {
        let single_card = $('.card[data-key=' + $('.card-details').attr('data-key') + ']');
        single_card.find('.doe-val').text(response.data['card']['doe']);

      }
      else {
        console.log(response.data);
        alert(SERVER_ERROR_MSG);
      }

    })
      .catch(function (error) {
        console.log(error);
        alert(SERVER_ERROR_MSG);
      });
  }

  $('body').on('change', '.doe-edit', function () {
    update_card($(this).parent().parent())

  });
  $('body').on('focusout', '.comments-val', function () {
    update_card($(this).parent())
  });

});









